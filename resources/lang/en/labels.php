<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'       => 'All',
        'yes'       => 'Yes',
        'no'        => 'No',
        'copyright' => 'Copyright',
        'custom'    => 'Custom',
        'actions'   => 'Actions',
        'active'    => 'Active',
        'buttons'   => [
            'save'   => 'Save',
            'update' => 'Update',
        ],
        'hide'               => 'Hide',
        'inactive'           => 'Inactive',
        'none'               => 'None',
        'show'               => 'Show',
        'toggle_navigation'  => 'Toggle Navigation',
        'create_new'         => 'Create New',
        'toolbar_btn_groups' => 'Toolbar with button groups',
        'more'               => 'More',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',
                'user_actions'        => 'User Actions',

                'table' => [
                    'confirmed'         => 'Confirmed',
                    'created'           => 'Created',
                    'email'             => 'E-mail',
                    'id'                => 'ID',
                    'last_updated'      => 'Last Updated',
                    'name'              => 'Name',
                    'first_name'        => 'First Name',
                    'last_name'         => 'Last Name',
                    'no_deactivated'    => 'No Deactivated Users',
                    'no_deleted'        => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions'       => 'Permissions',
                    'abilities'         => 'Abilities',
                    'roles'             => 'Roles',
                    'social'            => 'Social',
                    'total'             => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'        => 'Avatar',
                            'confirmed'     => 'Confirmed',
                            'created_at'    => 'Created At',
                            'deleted_at'    => 'Deleted At',
                            'email'         => 'E-mail',
                            'last_login_at' => 'Last Login At',
                            'last_login_ip' => 'Last Login IP',
                            'last_updated'  => 'Last Updated',
                            'name'          => 'Name',
                            'first_name'    => 'First Name',
                            'last_name'     => 'Last Name',
                            'status'        => 'Status',
                            'timezone'      => 'Timezone',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],

        'package' => [
            'package'     => 'Package System',
            'create'      => 'Create Package',
            'edit'        => 'Edit Package',
            'management'  => 'Package',
            'active'      => 'Active Package',
            'deactivated' => 'Deactivated Package',
            'deleted'     => 'Deleted Package',
            'view'        => 'View Package',
            'table'       => [
                'id'              => 'ID',
                'last_updated'    => 'Last Updated',
                'package_id'      => 'Package Id',
                'image'           => 'Image',
                'redirection_url' => 'Redirection Url',
                'created_at'      => 'Created At',
            ],
            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],

                'content' => [
                    'overview' => [
                        'image'           => 'Image',
                        'title'           => 'Title',
                        'created_at'      => 'Created At',
                        'deleted_at'      => 'Deleted At',
                        'redirection_url' => 'Redirection Url',
                        'last_updated'    => 'Last Updated',
                    ],
                ],
            ],
        ],

        
        'contact' => [
            'contact'     => 'Contacts',
            'management'  => 'Contact',
            'view'        => 'View Contact',
            'active'      => 'Active Contacts',
            'table'       => [
                'id'              => 'ID',
                'name'    => 'Name',
                'email'      => 'Email',
                'phone'           => 'Phone',
                'message' => 'Message',
                'created_at'      => 'Created At',
            ],
            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],

                'content' => [
                    'overview' => [
                        'image'           => 'Image',
                        'title'           => 'Title',
                        'created_at'      => 'Created At',
                        'deleted_at'      => 'Deleted At',
                        'redirection_url' => 'Redirection Url',
                        'last_updated'    => 'Last Updated',
                    ],
                ],
            ],
        ],

        'book' => [
            'package' => 'Package System',
            'view'    => 'View Package',
            'details'  => 'Enquiry Details',
            'management'  => 'Enquiry List',
            'table'   => [
                'package_id'   => 'Package/Test ID',
                'lab_location' => 'Lab Location',
                'csname'       => 'Customer Name',
                'csemail'      => 'Customer Email',
                'created_at'   => 'Enquiry Time',
            ],
            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],

                'content' => [
                    'overview' => [
                        'package_id'   => 'Package/Test ID',
                        'lab_location' => 'Lab Location',
                        'csname'       => 'Name',
                        'csemail'      => 'Email',
                        'csphone'      => 'Phone Number',
                        'csnote'      => 'Note',
                        'bookdate'      => 'Booking Date',
                        'price'      => 'Price',
                        'created_at'   => 'Enquiry Time',
                        'testincluded'   => 'Test Included',
                        'last_updated' => 'Last Updated',
                    ],
                ],
            ],
        ],

    ],

    'frontend' => [
        'auth' => [
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button'    => 'Register',
            'remember_me'        => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Submit',
        ],

        'passwords' => [
            'expired_password_box_title'      => 'Your password has expired.',
            'forgot_password'                 => 'Forgot Your Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'update_password_button'          => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Created At',
                'edit_information'   => 'Edit Information',
                'email'              => 'E-mail',
                'last_updated'       => 'Last Updated',
                'name'               => 'Name',
                'first_name'         => 'First Name',
                'last_name'          => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],
    ],
];
