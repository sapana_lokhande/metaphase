@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')
    <!-- donor-questionnaire -->
    <section class="donor-login" >
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="request-login">
                        <h2>Login or Request Login</h2>
                        <div class="donor-login-form">
                            <h3>@lang('labels.frontend.auth.login_box_title')</h3>
                            {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                                <div class="from-inner-login">
                                    {{ html()->text('email')
                                        ->class('form-control-login')
                                        ->placeholder(__('validation.attributes.frontend.email_placeholder'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                                </div>
                                <div class="from-inner-login">
                                    {{ html()->password('password')
                                        ->class('form-control-login')
                                        ->placeholder(__('validation.attributes.frontend.password_placeholder'))
                                        ->required() }}
                                </div>
                                <!-- <a href="#">Forget Password?</a> -->
                                <button>@lang('labels.frontend.auth.login_box_title')</button>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                    <div class="orange-leaves-login">
                        <div class="pulse">
                            <img src="images/leaves-login.png" alt="" >
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="login-new-user">
                        <h2>New User?</h2>
                        <p>If this is your first visit to Takes3 site, or you don't already have a username and password
                            to access our egg donor database, please click the button below.</p>
                          <p>We will respond as soon as possible.</p>
                         <button>Request Login</button>   
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- donor-questionnaire -->
@endsection
