@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<!-- donor-questionnaire -->
<section class="donor-questionnaire donor-questionnaire-how-to-works">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="how-donor-col-1">
                    <h1><strong>How It works</strong><br> Egg Donor</h1>
                    <div class="green-leaves-questionnaire ">
                        <div class="pulse">
                            <img data-aos="fade-up-right" src="images/Questionnaire.PNG" alt="" class="img-fluid desktop-view-how-it-works">
                            <img data-aos="fade-up-right" src="images/Object.PNG" alt="" class="img-fluid mobile-view-how-it-works">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="egg-donor-col-2">
                    <p>Thank you for your interest in Takes3's donor egg program. Egg donation is a gift that lasts
                        a lifetime. Each donor's generosity is rewarded in many ways. By becoming an egg donor, you
                        may help individuals or couples create the families they have always wanted. </p>
                    <p> The process requires a few weeks of diligence and attention but minimal discomfort or pain.
                    </p>
                    <p>Takes3 is not a medical clinic and no medical tests take place in our offices. If we are able
                        to take action on your application, you will have an interview and complete a more extensive
                        questionnaire. </p>
                    <p> Your profile will then be posted on our private database (accessible to Takes3 clients only)
                        for a recipient to select. This matching period can take a few days to a few months. </p>

                </div>
            </div>
            <div class="col-md-4">
                <div class="how-donor-col-3">
                    <p>Once selected, you will begin the medical screening at the recipients' clinic, which could be
                        in your home city or somewhere else in the US, if you are open to travel (travel is all
                        expenses paid in advance). </p>
                    <p>The egg donation process includes a medical screening and then a series of hormone
                        stimulation leading to the retrieval procedure. The whole process from screening to
                        retrieval takes about three months; the actual stimulation period lasts about twelve days
                        and is when most of the attention of the process is focused. </p>
                    <button class="read-egg-donor-faq-btn">Read Egg Donation FAQ</button>
                    <button class="apply-new-how-to-donor">Apply Now</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- donor-questionnaire -->
<!-- ask-question -->
<section class="ask-question-sec">
    <div class="containre-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="frequently-sec">
                    <h2>Frequently Asked Questions</h2>
                </div>
            </div>
            <div class="col-lg-6 order-3 ">
                <div class="eligiblity-sec-left">
                    <h3>What are the eligibility requirements to be an egg donor?</h3>
                    <!-- video -->
                    <div class="eligiblity-video">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <!-- video -->
                    <p>Egg donor candidates must be at least 21 and no older than 29. [If you have recently donated successfully and are between 30 and 32 years old, your application may be considered.] Applicants must be US citizens or legal residents, residing in the US.
                    </p>
                    <p>Egg donor candidates can be of any ethnic or religious background.They must have knowledge of their family medical history. Egg donor candidates must be physically fit with a BMI (body mass index) less than 27. [To calculate BMI, use this online BMI calculator.] Candidates must be non-smoking, in excellent health, mature, and responsible.</p>

                    <h3>How does Takes3 egg donation program work?</h3>
                    <p>Egg donor candidates must be at least 21 and no older than 29. [If you have recently donated successfully and are between 30 and 32 years old, your application may be considered.] Applicants must be US citizens or legal residents, residing in the US.
                    </p>
                    <p>Egg donor candidates can be of any ethnic or religious background.They must have knowledge of their family medical history. Egg donor candidates must be physically fit with a BMI (body mass index) less than 27. [To calculate BMI, use this online BMI calculator.] Candidates must be non-smoking, in excellent health, mature, and responsible.</p>

                    <h3>What are the eligibility requirements to be an egg donor?</h3>
                    <!-- video -->
                    <div class="eligiblity-video">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <!-- video -->
                    <p>Egg donor candidates must be at least 21 and no older than 29. [If you have recently donated successfully and are between 30 and 32 years old, your application may be considered.] Applicants must be US citizens or legal residents, residing in the US.
                    </p>
                    <p>Egg donor candidates can be of any ethnic or religious background.They must have knowledge of their family medical history. Egg donor candidates must be physically fit with a BMI (body mass index) less than 27. [To calculate BMI, use this online BMI calculator.] Candidates must be non-smoking, in excellent health, mature, and responsible.</p>

                    <h3>Am I eligible to be an egg donor if I am using birth control?</h3>
                    <p>Egg donor candidates must be at least 21 and no older than 29. [If you have recently donated successfully and are between 30 and 32 years old, your application may be considered.] Applicants must be US citizens or legal residents, residing in the US.
                    </p>
                    <p>Egg donor candidates can be of any ethnic or religious background.They must have knowledge of their family medical history. Egg donor candidates must be physically fit with a BMI (body mass index) less than 27. [To calculate BMI, use this online BMI calculator.] Candidates must be non-smoking, in excellent health, mature, and responsible.</p>

                    <h3>Do I need to live near one of your offices in order to work with your egg donation program?</h3>
                    <p>Egg donor candidates must be at least 21 and no older than 29. [If you have recently donated successfully and are between 30 and 32 years old, your application may be considered.] Applicants must be US citizens or legal residents, residing in the US.
                    </p>
                    <p>Egg donor candidates can be of any ethnic or religious background.They must have knowledge of their family medical history. Egg donor candidates must be physically fit with a BMI (body mass index) less than 27. [To calculate BMI, use this online BMI calculator.] Candidates must be non-smoking, in excellent health, mature, and responsible.</p>

                    <h3>What are the eligibility requirements to be an egg donor?</h3>
                    <p>Egg donor candidates must be at least 21 and no older than 29. [If you have recently donated successfully and are between 30 and 32 years old, your application may be considered.] Applicants must be US citizens or legal residents, residing in the US.
                    </p>
                    <p>Egg donor candidates can be of any ethnic or religious background.They must have knowledge of their family medical history. Egg donor candidates must be physically fit with a BMI (body mass index) less than 27. [To calculate BMI, use this online BMI calculator.] Candidates must be non-smoking, in excellent health, mature, and responsible.</p>


                </div>
            </div>
            <div class="col-lg-6 order-2 order-md-12">
                <div class="eligiblity-sec-right">
                    <div class="eligiblity-sec-inner-text">
                        <ul>
                            <li>What are the eligibility requirements to be
                                an egg donor?</li>
                            <li>How does Takes3 egg donation program work?</li>
                            <li>How long does the egg donation process take?</li>
                            <li>What medical procedures are involved?</li>
                            <li>What is the time commitment?</li>
                            <li>Where do I go for my medical procedures?</li>
                            <li>Am I eligible to be an egg donor if I am using
                                birth control?</li>
                            <li>Do I need to live near one of your offices in order to work with your egg donation program?</li>
                            <li> Do I need insurance to be an egg donor?</li>
                            <li>Does it cost anything to be an egg donor?</li>
                            <li>How do I apply?</li>
                            <li>What legal protection do egg donors have?</li>
                            <li>Do I meet my recipients?</li>
                            <li>What happens to donor eggs after retrieval?</li>
                            <li>What happens if I don’t pass my screening?</li>
                            <li>What are the risks and side effects of these drugs and procedures?</li>
                            <li>Can an egg donor participate more than once?</li>
                            <li>Do I find out if my donation results in a successful pregnancy?</li>
                            <li>What are Takes3 credentials? How is Takes3 different from other agencies?</li>
                            <li>How much money do egg donors earn?</li>
                        </ul>
                    </div>
                </div>
                <div class="apply-now-becon-donor">
                    <a href="#">Apply to Become An Egg Donor Now!</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ask-question -->
@endsection