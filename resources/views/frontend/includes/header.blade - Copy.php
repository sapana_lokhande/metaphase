    <!-- header -->
    <nav class="navbar navbar-expand-lg fixed-top main-nav">
        <a class="navbar-brand logo-color take3-slider1" href="{{URL::to('/')}}">Takes<span class="logo-color">3</span></a>
        <div class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon toggle-icon-slide" id="final_draft"></span>
        </div>
        <div class="collapse navbar-collapse form-inline ">
            <!-- left-nav -->
            <ul class="navbar-nav ml-auto navbar-right text-left slider-navbar-top">
                <li class="nav-item ">
                    <a class="nav-link logo-color" href="#">
                        What is Takes 3?
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link logo-color" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FAQs and Fees</a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">FAQs for Recipients</a>
                        <a class="dropdown-item" href="#">Fees (for Recipients)</a>
                        <a class="dropdown-item" href="#">FAQs for Donors</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link logo-color" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        In the News
                    </a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Practitioner Directory</a>
                        <a class="dropdown-item" href="#">Support Groups and Services</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link logo-color" href="#">Resources</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="btn fst-slide-btn take3-slider3 btn-color" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Questions?</a>
                    <div class="dropdown-menu drop-dron-ask-question" aria-labelledby="navbarDropdown">
                        <p>Send your questions to our
                            Donor Egg program manager</p>
                        <form>
                            <textarea placeholder="Your message"></textarea>
                            <a href="#">Send message</a>
                        </form>
                    </div>
                </li>

            </ul>
        </div>
        <!-- nav-toggle-mobile -->
        <div class="mobile-nav-toggle" id="mobile_toggle_nav" style="display: none;">
            <button type="button" class="close" aria-label="Close" id="close_toggle_nav">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3>Menu</h3>
            <ul class="navbar-nav ml-auto navbar-right text-left slider-navbar-top">
                <li class="nav-item ">
                    <a class="nav-link" href="#">
                        What is Takes 3?
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FAQs and Fees</a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">FAQs for Recipients</a>
                        <a class="dropdown-item" href="#">Fees (for Recipients)</a>
                        <a class="dropdown-item" href="#">FAQs for Donors</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        In the News
                    </a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Practitioner Directory</a>
                        <a class="dropdown-item" href="#">Support Groups and Services</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Resources</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="btn fst-slide-btn" style="color:#ffffff;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Questions?</a>
                    <div class="dropdown-menu drop-dron-ask-question" aria-labelledby="navbarDropdown">
                        <p>Send your questions to our
                            Donor Egg program manager</p>
                        <form>
                            <textarea placeholder="Your message"></textarea>
                            <a href="#">Send message</a>
                        </form>
                    </div>
                </li>

            </ul>
        </div>
        <!-- nav-toggle-mobile -->
    </nav>
    <!-- header -->
    <!-- scroll-nav -->
    <div class="bottomMenu" id="white_homepage_header" style="display:none">
        <nav class="navbar navbar-expand-lg bg-dark">
            <a class="navbar-brand scroll-navbar-brand" href="#">Takes<span>3</span></a>
            <div class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon" id="scoll_toggle_nav"></span>
            </div>

            <!-- left-nav -->
            <ul class="navbar-nav ml-auto navbar-right text-left hide-mobile-view">
                <li class="nav-item ">
                    <a class="nav-link" href="#">
                        What is Takes 3?
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FAQs and Fees</a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">FAQs for Recipients</a>
                        <a class="dropdown-item" href="#">Fees (for Recipients)</a>
                        <a class="dropdown-item" href="#">FAQs for Donors</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        In the News
                    </a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Practitioner Directory</a>
                        <a class="dropdown-item" href="#">Support Groups and Services</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Resources</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="btn fst-slide-btn" style="color: #ffffff;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Questions?</a>
                    <div class="dropdown-menu drop-dron-ask-question" aria-labelledby="navbarDropdown">
                        <p>Send your questions to our
                            Donor Egg program manager</p>
                        <form>
                            <textarea placeholder="Your message"></textarea>
                            <a href="#">Send message</a>
                        </form>
                    </div>
                </li>

            </ul>

            <!-- nav-toggle-mobile -->
            <div class="mobile-nav-toggle" id="scroll_mobile_toggle_nav" style="display: none;">
                <button type="button" class="close" aria-label="Close" id="close_toggle_nav_scroll">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Menu</h3>
                <ul class="navbar-nav ml-auto navbar-right text-left slider-navbar-top">
                    <li class="nav-item ">
                        <a class="nav-link" href="#">
                            What is Takes 3?
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FAQs and Fees</a>
                        <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">FAQs for Recipients</a>
                            <a class="dropdown-item" href="#">Fees (for Recipients)</a>
                            <a class="dropdown-item" href="#">FAQs for Donors</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            In the News
                        </a>
                        <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Practitioner Directory</a>
                            <a class="dropdown-item" href="#">Support Groups and Services</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Resources</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn fst-slide-btn" style="color: #ffffff;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Questions?</a>
                        <div class="dropdown-menu drop-dron-ask-question" aria-labelledby="navbarDropdown">
                            <p>Send your questions to our
                                Donor Egg program manager</p>
                            <form>
                                <textarea placeholder="Your message"></textarea>
                                <a href="#">Send message</a>
                            </form>
                        </div>
                    </li>

                </ul>
            </div>
            <!-- nav-toggle-mobile -->
        </nav>
    </div>
    <!-- <scroll-nav -->