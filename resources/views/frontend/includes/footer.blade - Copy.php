    <!-- footer -->
    <footer>
        <!-- desktop-view -->
        <div class="container-fluid desktop-view-footer">
            <div class="row">
                <div class="col-lg-2 col-md-12">
                    <div class="footer-logo">
                        <a class="navbar-brand-footer " href="#">Takes<span>3</span></a>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-12 ">
                    <div class="footer-social">
                        <ul class="footer-nav-social">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        </ul>
                        <ul class="footer-nav-social">
                            <li><a href="#"><img class="instagram-footer" src="/images/instagram.png" alt=""></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="footer-middle">
                        <ul class="navbar-nav mr-auto footer-nav-navbar">
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">WHAT IS TAKES 3 ?</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">IN THE NEWS</a>
                            </li>
                        </ul>
                        <ul class="navbar-nav mr-auto footer-nav-navbar">
                            <li class="nav-item">

                                <a class="nav-link footer-nav-link" href="#">FAQ AND FEES</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">RESOURCES</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="copyright">
                        <p>© 2020 Takes3. All Rights Reserved </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- desktop-view -->
        <!-- mobile-view -->
        <div class="container-fluid mobile-view-footer">
            <div class="row">
                <div class="col-lg-2 col-md-12">
                    <div class="footer-logo">
                        <a class="navbar-brand-footer " href="#">Takes<span>3</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="footer-middle">
                        <ul class="navbar-nav mr-auto footer-nav-navbar">
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">About us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">FAQs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">In The News</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link footer-nav-link" href="#">Resources</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-12 ">
                    <div class="footer-social">
                        <ul class="footer-nav-social">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li><a href="#"><img class="instagram-footer" src="/images/instagram.png" alt=""></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="copyright">
                        <p>© 2020 Takes3. all rights reserved. </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-view -->
    </footer>
    <!-- footer -->

    {!! script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js') !!}
    {!! script('https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js') !!}
    {!! script('https://unpkg.com/aos@2.3.1/dist/aos.js') !!}
    {!! script('js/custome.js') !!}