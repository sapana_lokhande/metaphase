<!-- header -->
    <nav class="navbar navbar-expand-lg fixed-top inner-pages-nav box-shadow-white">
        <a class="navbar-brand logo-color take3-slider1" href="{{URL::to('/')}}">Takes<span class="logo-color">3</span></a>
        <div class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </div>
        <div class="collapse navbar-collapse form-inline " id="navbarSupportedContent">
            <!-- left-nav -->
            <ul class="navbar-nav ml-auto navbar-right text-left outo-top-crol">
                <li class="nav-item dropdown">
                    <a class="nav-link logo-color" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        What is Takes 3?
                    </a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Our Mission</a>
                        <a class="dropdown-item" href="#">How it Works</a>
                        <a class="dropdown-item" href="#">Who we are</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link logo-color" href="#">FAQs and Fees</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link logo-color" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        In the News
                    </a>
                    <div class="dropdown-menu aerrow-box" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Blog</a>
                        <a class="dropdown-item" href="#">Recent Articles</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link logo-color" href="#">Resources</a>
                </li>
                <li class="nav-item">
                    <button class="btn btn-color">Questions?</button>
                </li>

            </ul>
        </div>
    </nav>