<div class="header-classic">
	<!-- navigation start -->
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <nav class="navbar navbar-expand-lg navbar-classic">
                    <a class="navbar-brand 1" href="/">
                        <img src="{{ asset('v1/images/metaphase-orange.png') }}" class="logo img-responsive" />
                    </a>

                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                        data-target="#navbar-classic" aria-controls="navbar-classic" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="icon-bar top-bar mt-0"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse " id="navbar-classic">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0 mr-3">
							<li class="nav-item">
                                <a class="nav-link" href="">Home</a>
                            </li>
                            <li class="nav-item dropdown mega-dropdown">
                                <a class="nav-link dropdown-toggle" href="" id="menu-4" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    Services
                                </a>
                                <ul class="dropdown-menu mega-dropdown-menu" aria-labelledby="menu-4">
                                    <li class="row">
                                        <ul class="col">
                                            <li><a href="#" class="main-heading dropdown-item">COVID Testing</a>
                                            </li>
                                            <li><a href="#" class="main-heading dropdown-item">Home Sample
                                                    Collection</a></li>
                                            <li>
                                                <div class="main-heading">Radiology</div>
                                                <a href="#" class="dropdown-item">Pre-natal Support</a>
                                            </li>
                                            <li>
                                                <div class="main-heading">Cardiology</div>
                                                <a href="#" class="dropdown-item">TMT(Tread Mill Test)</a>
                                                <a href="#" class="dropdown-item">ECHO Doppler</a>
                                                <a href="#" class="dropdown-item">ECG</a>
                                            </li>
                                            <li><a href="ultrasound-scan.html" class="dropdown-item">Ultrasound Scan(3D
                                                    &amp; 4D)</a>
                                            </li>
                                            <li>
                                                <div class="main-heading">Bone Densitometry(BDM)</div>
                                                <div class="main-heading">Digital X-Ray</div>
                                                <a href="#" class="dropdown-item">Opthamology</a>
                                            </li>

                                        </ul>
                                        <ul class="col">
                                            <li>
                                                <div class="main-heading">Clinical Support</div>
                                                <a href="#" class="dropdown-item">Pediatric Counselling</a>
                                                <a href="#" class="dropdown-item">Pre-natal Diagnostics Counselling</a>
                                                <a href="#" class="dropdown-item">Diet Counselling</a>
                                                <a href="#" class="dropdown-item">Psychiatric Counselling</a>
                                                <a href="#" class="dropdown-item">Depression Counselling</a>
                                                <a href="#" class="dropdown-item">Psychology Counselling</a>
                                                <a href="#" class="dropdown-item">Physiotherapy and Fitness
                                                    Counselling</a>
                                                <a href="#">
                                                    <font style="vertical-align: inherit;" class="dropdown-item">
                                                        <font style="vertical-align: inherit;">Dementia Care</font>
                                                    </font>
                                                </a>
                                                <a href="#" class="dropdown-item">Dentistry</a>
                                            </li>

                                        </ul>
                                        <ul class="col">
                                            <li>
                                                <div class="main-heading">Diagnostics</div>
                                                <a class="dropdown-item" href="#">
                                                    block 13</a>
                                                <a href="#" class="dropdown-item">New Born Baby Screening</a>
                                                <a href="#" class="dropdown-item">Pathology</a>
                                                <a href="#" class="dropdown-item">Biochemistry</a>
                                                <a href="#" class="dropdown-item">Serology and Microbiology</a>
                                                <a href="#" class="dropdown-item">Haematology</a>
                                                <a href="#" class="dropdown-item">Cytopathology</a>
                                                <a href="#" class="dropdown-item">Histopathology</a>
                                                <a href="#" class="dropdown-item">Molecular Diagnostic</a>


                                            </li>
                                        </ul>

                                    </li>
                                </ul>
                            </li>
                            <!--<li class="nav-item">
                                <a class="nav-link" href="#">Exclusive Offers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Corporate's Corners</a>
                            </li>-->
                            <li class="nav-item">
                                <a class="nav-link" href="contact">Contact</a>
                            </li>
                        </ul>

                        <!--<a href="#" class="btn btn-brand btn-rounded btn-sm"><img
                                src="{{ asset('v1/images/icon/upload-prescription-icon.png') }}" class="pr-10" /><span
                                class="ipad-land-none">Upload Prescription</span></a>
                        <a href="#" class="btn btn-brand btn-rounded btn-sm"><img
                                src="{{ asset('v1/images/icon/sign-up-icon.png') }}" class="pr-10" /><span
                                class="ipad-land-none">Sign Up</span></a>-->
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- navigation close -->
</div>
<!--end header-->
