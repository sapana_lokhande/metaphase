    <!-- donor-slider -->
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 p0">
                    <div id="demo" class="carousel slide" data-ride="carousel">
                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>
                            <li data-target="#demo" data-slide-to="1"></li>
                            <li data-target="#demo" data-slide-to="2"></li>
                        </ul>
                        <div class="carousel-inner">
                            <!-- slider-1 -->
                            <div class="carousel-item active">
                                <div class="bg-slider-img-1">
                                    <div class="carousel-caption">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 responisive-padding-0">
                                                <h4 data-aos="fade-right" data-aos-anchor="#example-anchor"
                                                    data-aos-offset="500" data-aos-duration="1000">Where Recipients
                                                    And<br> Donors Connect</h4>
                                                <div class="slider-text">
                                                    <p data-aos="fade-right" data-aos-anchor="#example-anchor"
                                                        data-aos-offset="500" data-aos-duration="1500"><a
                                                            class="onslider-logo" href="#">Takes<span>3</span></a>is a
                                                        comprehensive
                                                        platform for egg<br> donation where you can</p>
                                                </div>
                                                <div data-aos="fade-right" data-aos-anchor="#example-anchor"
                                                    data-aos-offset="500" data-aos-duration="2000" class="find-donor">
                                                    Find Your Ideal Donor<br>
                                                    Become an Egg Donor<br>
                                                    Get information, support, and inspiration
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 order-first order-md-2 mobile-view-top-margin"
                                                data-aos="zoom-in-left">
                                                <img src="images/leaves.png" alt="" class="img-fluid pulse">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- slider-2 -->
                            <div class="carousel-item ">
                                <div class="bg-slider-img-2">
                                    <div class="carousel-caption">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <h3>Become an Egg Donor</h3>
                                                <button onclick="location.href='/donor/questionnaire'">APPLY NOW</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- slider-3 -->
                            <div class="carousel-item">
                                <div class="bg-slider-img-3">
                                    <div class="carousel-caption">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <h3>Find an Egg Donor</h3>
                                                <button onclick="location.href='/login'">SEARCH DONOR PROFILES</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- donor-slider -->
