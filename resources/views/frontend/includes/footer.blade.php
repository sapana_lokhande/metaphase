<footer class=" footer-section ">
    <!-- Footer Elements -->
    <div class=" container ">
        <!-- Grid row-->
        <div class=" row ">
            <div class=" col-md-8 col-sm-6 col-xs-12 w-50-1">
                <h3>Metaphase Medical<br /> Dignostics private limited</h3>

                <div class=" col-md-1 pl-0"><i class=" fa fa-phone "></i></div>
                <div class=" col-md-11 ">
                    <a href=" tel:+91 63097 38222">+91 63097 38222</a><br/>
					<a href=" tel:+91 98450 17696">+91 98450 17696</a>
                </div>
                <div class="clearfix mb-20"></div>
                <div class=" col-md-1 pl-0"><i class=" fa fa-envelope "></i></div>
                <div class=" col-md-11 ">
                    <a href=" mailto:we@metaphase.in ">we@metaphase.in</a>
                </div>

            </div>
            <div class="mob-5 col-md-4 col-sm-6 col-xs-12 w-50-1">
                <h3 class="mob-3">Operational Hours</h3>
                <div class="row">
                    <div class=" col-md-4 col-sm-4 col-xs-6">
                        <h6>Weekdays</h6>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-6">
                        <h6>6am - 11pm</h6>
                    </div>
                </div>
                <div class="mb-20"></div>
                <div class="row">
                    <div class=" col-md-4 col-sm-4 col-xs-6">
                        <h6>Sunday</h6>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-6">
                        <h6>6am - 2pm</h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-50 clearfix"></div>
        <div class="row">
            <!-- Grid column -->
            <div class=" col-md-3 col-sm-6 col-xs-12 w-50-1">
                <h4>Services</h4>
                <ul>
                    <li><a href=" # ">Covid test</a></li>
                    <li><a href=" # ">Home sample collection</a></li>
                    <li><a href=" # ">Radiology</a></li>
                    <li><a href=" # ">Pre natal support</a></li>
                    <li><a href=" # ">Cardiology</a></li>
                    <li><a href=" # ">TMT (Treadmill test)</a></li>
                    <li><a href=" # ">echo doppler</a></li>
                    <li><a href=" # ">ecg</a></li>
                    <li><a href=" # ">ultrasound scan 3d & 4d</a></li>
                    <li><a href=" # ">Bone Densitometry(BDM)</a></li>
                    <li><a href=" # ">Digital X-Ray</a></li>
                    <li><a href=" # ">Opthamology</a></li>
                </ul>
            </div>
            <div class=" col-md-3  col-sm-6 col-xs-12 w-50-1">
                <h4>Clinical Support</h4>
                <ul>
                    <li><a href=" # ">Pediatric Counselling</a></li>
                    <li><a href=" # ">Pre-natal Diagnostics Counselling</a></li>
                    <li><a href=" # ">Diet Counselling</a></li>
                    <li><a href=" # ">Psychiatric Counselling</a></li>
                    <li><a href=" # ">Depression Counselling</a></li>
                    <li><a href=" # ">Psychology Counselling</a></li>
                    <li><a href=" # ">Physiotherapy and Fitness Counselling</a></li>
                    <li><a href=" # ">Dementia Care</a></li>
                    <li><a href=" # ">Dentistry</a></li>

                </ul>
            </div>
            <div class=" col-md-3 col-sm-6 col-xs-12 w-50-1">
                <h4>Diagnostics</h4>
                <ul>
                    <li><a href=" # ">block 13</a></li>
                    <li><a href=" # ">New Born Baby Screening</a></li>
                    <li><a href=" # ">Pathology</a></li>
                    <li><a href=" # ">Biochemistry</a></li>
                    <li><a href=" # ">Serology and Microbiology</a></li>
                    <li><a href=" # ">Haematology</a></li>
                    <li><a href=" # ">Cytopathology</a></li>
                    <li><a href=" # ">Histopathology</a></li>
                    <li><a href=" # ">Molecular Diagnostic</a></li>

                </ul>
            </div>
            <!-- Grid column -->
            <div class=" col-md-3 col-sm-6 col-xs-12 w-50-1">
                <h4>Follow Us</h4>
                <div class=" socialicon ">
                    <ul>
                        <li><a href="#" target=" _blank "><i class="fa fa-facebook"
                                    class=" img-responsive "></i></a></li>
                        <li><a href="#" target=" _blank "><i class="fa fa-instagram"
                                    class=" img-responsive "></i></a></li>
                        <li><a href="#" target=" _blank "><i class="fa fa-twitter" class=" img-responsive "></i></a>
                        </li>
                    </ul>
                </div>
                <div class=" clearfix "></div>
                <br>
            </div>

        </div>
        <!-- Grid row-->
    </div>
    <!-- Footer Elements -->
    <!-- Copyright -->
    <div class=" footer-copyright ">
        <div class="container">
            <div class="col-md-12">
                &copy; 2020, Metaphase Medical Diagnostic Pvt Ltd. All Rights Reserved.
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- Copyright -->
</footer>