@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="static-slider4">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <div class="col-md-6 col-sm-6 img-anim">
                    <img src="{{ asset('v1/images/banner/package-details-banner.jpg') }}" alt="wrapkit" class="img-fluid" />

                </div>
                <!-- Column -->
                <div class="col-md-6 col-sm-6 ml-auto align-self-top mt-8">
                    {{-- {{ dd($packageInfo) }} --}}
                    <h1 class="title animated fadeInRight">{{ $packageInfo['testName'] }}</h1>
                    <h2 class="h2-tranform">{{ count($packageInfo['testList']) }} Tets Included</h2>
                    <h2 class="h2-tranform">400+ Booked In Last 7 Days</h2>

                    <div class="clearfix"></div>
                    <!--<a class="btn btn-outline-info btn-md mt-2 animated fadeInUp" href="">
                                <span>Know More </span>
                            </a>-->

                </div>
                <!-- Column -->
            </div>
        </div>
    </div>
    <!--banner Image end-->
    <section class="package-cover-section price-section">
        <div class=" container ">
            <div class=" row ">
                <div class="col-md-4 text-center">
                    <img src="{{ asset('v1/images/tick-icon.png') }}" alt="wrapkit" class="img-fluid" />
                    <h4>Free consultation with top doctors</h4>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ asset('v1/images/tick-icon.png') }}" alt="wrapkit" class="img-fluid" />
                    <h4>Sample Pickup at home</h4>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ asset('v1/images/tick-icon.png') }}" alt="wrapkit" class="img-fluid" />
                    <h4>online reports within 48 hours</h4>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section class="package-cover-section price-section">
        <div class=" container ">
            <div class=" row ">


                <div class="col-md-12 text-center col-sm-12 ml-auto align-self-top">
                    <h1>This Package Covers</h1>
                </div>
                <div class="div-center">
                    @php
                    $chunk_list = [];
                    if(!empty($packageInfo['testList']))
                    {
                    	$chunk_list = array_chunk($packageInfo['testList'],6);
                    }
                    @endphp

                    @foreach ($chunk_list as $chunk_item)
                        <div class="col-md-4 col-sm-6">
                            <ul class="list p-0">
                                @foreach ($chunk_item as $item)
                                    <li>{{ $item['testName'] }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>

                </div>
                <div class="div-center1">
                    <div class="col-md-12 col-sm-12 ml-auto align-self-top">
                        <ul class="listA p-0">
                            <li>Metaphass will allocate a certified lab technician 12-24 hours before your scheduled slot
                            </li>
                            <li>Samples will be collected at your house by the Healthians phlebotimist in the scheduled slot
                            </li>
                            <li>Your reports will be uploaded on the app within 48 hours of sample collection</li>
                            <li>Online consultation with a senior doctor is scheduled for you along with the reports</li>
                            <li>Based on your results, our senior doctor will share their advice & a personalized
                                prescription</li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>
                    <div class="mt-8"></div>
                    <div class="col-md-6 col-sm-12 column">
                        <h1 class="textleft">{{ $packageInfo['testName'] }}</h4>
                    </div>
                    <div class="col-md-6 col-sm-12 column">
                        <div class="mob-left pull-right mt-4">
                            <h1><i class="f-r fa fa-rupee"></i> {{ $packageInfo['testAmount'] }}</h1>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mt-8"></div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary book-test book-btn">Book Now</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('after-scripts')
    <script>
        $(function() {
            $(".book-btn").on('click', function() {

                let route =
                    "{{ route('frontend.book.index', ['type' => $type, 'check' => $check, 'center' => $center]) }}";
                window.location.href = route;
            })
        })

    </script>
@endpush