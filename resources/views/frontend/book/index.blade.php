@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="contact-page">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10 col-md-12 ipad-landscape-w col-sm-12 col-xs-12">
                    <div class="package-details table-responsive mt-6">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="package-heading">Package Details</th>
                                    <th class="package-heading">Price</th>
                                    <th class="package-heading">Test</th>
                                    <th class="package-heading">Total Payble</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $packageInfo['testName'] }}</td>
                                    <td>{{ $packageInfo['testAmount'] }}</td>
                                    @if (!empty($packageInfo['testList']))
                                        <td>{{ count($packageInfo['testList']) }}</td>
                                    @else
                                        <td>0</td>
                                    @endif
                                    <td class="total">{{ $packageInfo['testAmount'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--table end-->
                    </div>
                    <!--end table responsive div-->
                </div>
                <!--end col-md-12-->
                <div class="clearfix"></div>
                <div class="col-lg-6 col-md-12 col-sm-12 img-anim mt-4">
                    @if (!empty($packageInfo['testList']))
                        <h1 class="title animated fadeInRight">Test List</h1>
                        <div class="clearfix"></div>
                        <div class="row mt-4">
                            @php
                            $chunk_list = [];
                            if(!empty($packageInfo['testList']))
                            {
                            $chunk_list = array_chunk($packageInfo['testList'],6);
                            }
                            @endphp

                            @foreach ($chunk_list as $chunk_item)
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <ul class="list p-0">
                                        @foreach ($chunk_item as $item)
                                            <li>{{ $item['testName'] }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!--end col-md-6-->
                            @endforeach
                            <!--<img src="{{ asset('v1/images/banner/book-here-banner.jpg') }}" alt="wrapkit" class="img-fluid" />-->

                            {{-- <li>Lipid Profile</li>
                            <li>Liver Function Test</li>
                            <li>Thyroid Profile</li>
                            <li>Iron Studies</li>
                            <li>Kidney Function Test</li> --}}


                        </div>
                        <!--end row-->
                    @endif

                </div>
                <!--end col-md-6-->
                <!-- Column -->
                <div class="mob-clearfix col-lg-6 col-md-12 col-sm-12 ml-auto align-self-top mt-4">

                    <h1 class="title animated fadeInRight">Patient Details</h1>
                    <!--form start-->
                    <form id="bookPTForm" enctype="multipart/form-data">

                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 pl-0">
                                <label for="customerName">Name</label>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" id="customerName" name="customerName"
                                    placeholder="Enter your name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 pl-0">
                                <label for="customerEmail">Email</label>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <input type="email" class="form-control" id="customerEmail" name="customerEmail"
                                    placeholder="Enter your email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 pl-0">
                                <label for="customerPhone">Phone</label>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control" id="customerPhone" name="customerPhone"
                                    placeholder="Enter your phone number" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 pl-0">
                                <label for="customerBookDT">Booking Date</label>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <div class='input-group date' id='customerBookDTPicker'>
                                    <input class="form-control" id="customerBookDT" name="customerBookDT" type="text"
                                        value="" placeholder="Select booking date" readonly>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 pl-0">
                                <label for="customerNote">Note</label>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <textarea class="form-control" id="customerNote" name="customerNote" rows="3"
                                    placeholder="Enter your note"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="submitbtn btn btn-primary book-test">Submit</button>
                        <div>
                            <small id="form_message"></small>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                    <!--form end-->


                </div>
                <!--end col-md-6-->
                <!-- Column -->
            </div>
            <!--end row-->

        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@push('after-scripts')
    {!! script('v1/js/bootstrap-datetimepicker.min.js') !!}
    <script>
        $(function() {
            $('#customerBookDTPicker').datetimepicker({
                minDate: new Date()
            });

            $('#bookPTForm').submit(function() {
                event.preventDefault();
                $(".book-test").prop("disabled", true);

                let formData = new FormData($(this)[0]);
                formData.append("type", '{{ $type }}');
                formData.append("center", '{{ $center }}');
                formData.append("check", '{{ $check }}');

                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "{{ route('frontend.book.bookpt') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        $('#form_message').html(data.message);
                        $('#form_message').addClass('text-success');
                        setTimeout(function() {
                            window.location.href = "{{ route('frontend.index') }}";
                        }, 3000);
                    },
                    error: function(e) {
                        $('#form_message').html(e.responseJSON.message);
                        $(".book-test").prop("disabled", false);
                        $('#form_message').addClass('text-danger');
                    },
                    always: function(e) {
                        $(".book-test").prop("disabled", false);
                    }
                });
            });
        })

    </script>
@endpush

@push('after-styles')
    {{ style('v1/css/bootstrap-datetimepicker.min.css') }}
@endpush
