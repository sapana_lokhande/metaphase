@extends('frontend.layouts.app')
@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')
    <!-- header -->
    <!-- donor-questionnaire -->
    <section class="donor-search">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 mobil-padd-0 padding-left-0">
                    <div class="donor-search-navigation">
                        <ul>
                            <li><a href="#" class="active"><i class="fa fa-search" aria-hidden="true"></i> Search Donors</a></li>
                            <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i> My Favorite Profiles</a></li>
                            <li><a href="{{URL::to('/logout')}}"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign Out</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="donor-profile-search">
                        <h3>Donor Profile</h3>
                        <p>Search Donors</p>
                        <div class="donor-search-form">
                            <form>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <input type="password" class="form-donor-search" placeholder="Donor Code">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <select class="form-donor-search">
                                                <option>Geographical Profile</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <select class="form-donor-search">
                                                <option>Eye Color</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <select class="form-donor-search">
                                                <option>Hair Color</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <select class="form-donor-search">
                                                <option>Ethnicity</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <input type="password" class="form-donor-search" placeholder="Height">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="from-inner-login">
                                            <select class="form-donor-search">
                                                <option>Education</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="checkbox-form-donor">
                                                <input type="checkbox" value="Repeat Donor">Repeat Donor
                                            </label>
                                            <label class="checkbox-form-donor">
                                                <input type="checkbox" value="Repeat Donor">Repeat Donor
                                            </label>
                                            <label class="checkbox-form-donor">
                                                <input type="checkbox" value="Repeat Donor">Repeat Donor
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="search-dbore-btn">
                                            <button>Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- donor-profile -->
    <section class="donor-data-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="clearfix">
                        <div class="float-left">
                            <div class="show-number-drop">
                                <span>Show</span>
                                <select name="hg" class="outer-drop-number">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                </select>
                            </div>
                        </div>
                        <div class="btn-group float-right">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="boxes-search-donor">
                        <div class="row">
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart light-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart dark-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                    <div class="reserved-donor">
                                        <span>Reserved</span>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart light-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart light-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                    <div class="repeat-donor">
                                        <span>Repeat Donor</span>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart dark-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                    <div class="reserved-donor">
                                        <span>Reserved</span>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart light-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart light-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                    <div class="repeat-donor">
                                        <span>Repeat Donor</span>
                                    </div>
                                </div>
                            </div>
                            <!-- profile-card -->
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                                <div class="card">
                                    <span class="card-img-top"></span>
                                    <div class="card-body">
                                        <h5 class="card-title">CN93.33</h5>
                                        <span>27 Year Old Caucasian<br> (English-Scottish, German).<br>
                                            NY Metro Area.
                                        </span>
                                        <p class="card-text">Some quick example text to build on the card title and
                                            make.</p>
                                        <div class="clearfix">
                                            <div class="float-left">
                                                <button class="view-full-profile-btn">View Full Profile</button>
                                            </div>
                                            <div class="float-right"><i class="fa fa-heart light-heart"
                                                    aria-hidden="true"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="bottom-pagination">
                        <div class="btn-group">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection