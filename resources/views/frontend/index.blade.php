@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="static-slider4">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <div class="col-md-7 col-sm-6 img-anim">
                    <img src="v1/images/banner/home-banner-image.jpg" alt="wrapkit" class="img-fluid" />
                </div>
                <!-- Column -->
                <div class="col-md-5 col-sm-6 ml-auto align-self-top mt-8">
                    <div class="plr">
                        <h1 class="title animated fadeInRight">The race to finding a covid-19 drug in the blood of survivors
                        </h1>
                        <p class="mt-3 in-right">to good laboratories, these best practices become routine procedures; to
                            good analysts they
                            become habits. the goal is to produce quality results.
                        </p>
                        <div class="clearfix"></div>
                        <!--<a class="btn btn-outline-info btn-md mt-2 animated fadeInUp" href="">
                            <span>Know More </span>
                        </a>-->
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div>
    </div>
    <!--banner Image end-->
    <!--price slider-->
    <section class=" price-section carousel1 ">
        <div class=" container ">
            <div class=" row ">
                <form method="GET" action="{{ route('frontend.search.package') }}">
                    <div class="row">
						<div class=" col-md-2 ml-4 "></div>
                        <div class="form-group mb-70 col-md-3 col-sm-5 col-xs-12 w-30">
                            <!--dropdown start-->
                            <select id="location" name="location" class="dropp-header1 form-control">
                                @foreach ($labCenters as $centerId => $location)
                                    <option value="{{ $centerId }}">{{ $location }}</option>
                                @endforeach
                            </select>
                            <!--dropdown end-->
                        </div>
                        <div class="form-group mb-70  col-md-3 col-sm-5 col-xs-12 w-30">
                            <input type="text" placeholder=" Find your package / test " class="dropp-header1 form-control" name="packagetest">
                        </div>
                        <div class="form-group mb-70 col-md-1 col-sm-2 col-xs-12">
                            <button type="submit"  class=" sb-icon-search1 btn btn-primary"></button>
                        </div>
						</div>
                  
                </form>

                <div class=" clearfix "></div>
                <div class=" col-md-12 ">
                    <h1>Full Body Check-Up Packages</h1>
                    <div class="clearfix"></div>
                    <div class=" bbb_viewed_title_container ">
                        <!--Controls-->
                        <div class=" bbb_viewed_nav_container ">
                            <div class=" bbb_viewed_nav bbb_viewed_prev "> <a class=" btn-floating "
                                    href=" #multi-item-example " data-slide=" prev "><i class=" fa fa-caret-left "></i></a>
                            </div>
                            <div class=" bbb_viewed_nav bbb_viewed_next "><a class=" btn-floating "
                                    href=" #multi-item-example " data-slide=" next "><i class=" fa fa-caret-right "></i></a>
                            </div>
                        </div>
                    </div>
                    <div class=" bbb_viewed_slider_container ">
                        <div class=" owl-carousel owl-theme bbb_viewed_slider">
                            <?php foreach($packages as $key => $value) {
                                // dd($value); ?>
                            <div class=" owl-item ">
                                <div class=" block block-pricing ">
                                    <div class=" table ">
                                        <h6 class="category ">{{ $value['testName'] }}</h6>
                                        <ul class=" list-ul ">
                                        <?php 
                                        if(count($value['testList']) > 0) 
                                        {
                                            $i = 0;
                                            foreach($value['testList'] as $key1 => $value1) 
                                            { 
                                                // dd($value1['testName']); ?>
                                            <li>{{ $value1['testName'] }}</li>
                                            
                                            <?php 
                                                if (++$i > 2){
                                                    break;
                                                }
                                            }
                                        } else {
                                        ?>
                                            <li>Include blood glucose fating</li>
                                            <li>Hba1c</li>
                                            <li>Kidney function test</li>
                                        <?php }?>
                                        </ul>
                                        <h1 class=" block-caption "><i class=" fa fa-rupee "></i> {{ $value['testAmount'] }}</h1>
                                        {{-- <a class="btn-round1 btn detail-btn" id="package_{{ $value['testID'] }}">View Details</a> --}}
                                        <button type="button" id="package_{{ $value['testID'] }}" class="btn btn-danger btn-round btn-lg btn-block btn-outline-primary book-btn ">TEST NOW</button>
                                    </div>
                                </div>
                            </div>
                            <!--end box-->
                            <?php } ?>

                            <!--end box-->
                            <!-- <div class=" owl-item ">
                                <div class=" block block-pricing ">
                                    <div class=" table ">
                                        <h6 class=" category ">Meta diabetes checkup</h6>
                                        <ul class=" list-ul ">
                                            <li>Include blood glucose fating</li>
                                            <li>Hba1c</li>
                                            <li>Kidney function test</li>
                                        </ul>
                                        <h1 class=" block-caption "><i class=" fa fa-rupee "></i> 999</h1>
                                        <a href=" # " class=" btn btn-danger btn-round ">Test Now</a>
                                    </div>
                                </div>
                            </div> -->
                            <!--end box-->
                            <!-- <div class=" owl-item ">
                                <div class=" block block-pricing ">
                                    <div class=" table ">
                                        <h6 class=" category ">Meta diabetes checkup</h6>
                                        <ul class=" list-ul ">
                                            <li>Include blood glucose fating</li>
                                            <li>Hba1c</li>
                                            <li>Kidney function test</li>
                                        </ul>
                                        <h1 class=" block-caption "><i class=" fa fa-rupee "></i> 999</h1>
                                        <a href=" # " class=" btn btn-danger btn-round ">Test Now</a>
                                    </div>
                                </div>
                            </div> -->
                            <!--end box-->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div class=" clearfix "></div>
    <!--end price slider-->
    <section class=" black-section">
        <div class=" container ">
            <div class=" row ">
                <div class=" col-md-12 ">
                    <h1 style="margin-bottom:30px;">Free Home Sample Collection</h1>
					<h2 class="text-center" style="color:#fff; margin-bottom:50px;"><span style="color:#E76230;">Contact Us :</span> +91 6309738222</h2>
                </div>
                
                <div class=" clearfix "></div>
            </div>
        </div>
    </section>
    <div class=" clearfix "></div>
    <section class=" white-section ">
        <div class=" container ">
            <div class=" row ">
                <div class=" col-md-6 col-sm-6 col-xs-12">
                    <img src="v1/images/lifestyle.jpg" class="img-responsive center-block" />
                </div>
                <div class=" col-md-6 col-sm-6 col-xs-12">
                    <div class=" padding-lr ">
                        <h1>Lifestyle</h1>
                        <p>A person's health is Influenced by health behaviours that are part of their indivisual lifestyle
                            a person's health is Influenced by health behaviours that are part of their indivisual
                            lifestyle.</p>
                        <a class=" btn btn-outline-info btn-md mt-2 animated fadeInUp " href="#" onclick="return false"><span>Reach Our Experts <i
                                    class=" fa fa-angle-right pl-10 "></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class=" clearfix "></div>
    <section class="black-section1 black-section carousel2">
        <div class=" container ">
            <div class=" row ">
                <div class=" col-md-12 ">
                    <div class="bbb_viewed_title_container">

                        <div class="bbb_viewed_nav_container">
                            <div class="bbb_viewed_nav bbb_viewed_prev1 bbb_viewed_prev"><i class="fa fa-circle"></i></div>
                            <div class="bbb_viewed_nav bbb_viewed_next1 bbb_viewed_next"><i class="fa fa-circle"></i></div>
                        </div>
                    </div>
                    <div class=" bbb_viewed_slider_container">
                        <div class=" owl-carousel owl-theme bbb_viewed_slider ">
                            <div class=" owl-item active ">
                                <div class=" shadow-box ">
                                    <img class="svg" src=" v1/images/icon-svg/Heart.svg " />

                                </div>
                                <h2>Heart</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                            <div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src=" v1/images/icon-svg/Hypertension.svg" />
                                </div>
                                <h2>Hypertension</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                            <div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src=" v1/images/icon-svg/Joints.svg " />
                                </div>
                                <h2>Joints</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                            <div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src=" v1/images/icon-svg/junk-food.svg" />
                                </div>
                                <h2>Junk Food</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                            <div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src=" v1/images/icon-svg/kidneys.svg " />
                                </div>
                                <h2>Kidneys</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                            <div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/lifestyle.svg " />
                                </div>
                                <h2>Lifestyle</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
							<div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/fever.svg " />
                                </div>
                                <h2>Fever</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
							<div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/diabetes.svg " />
                                </div>
                                <h2>Diabetes</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
							<div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/cancer.svg " />
                                </div>
                                <h2>Cancer</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
							<div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/blood-test.svg " />
                                </div>
                                <h2>Blood Test</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
							<div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/Anemia.svg " />
                                </div>
                                <h2>Anemia</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                           <div class=" owl-item ">
                                <div class=" shadow-box ">
                                    <img class="svg" src="v1/images/icon-svg/Alcoholism.svg " />
                                </div>
                                <h2>Alcoholism</h2>
                                <p>A persons health is influenced by health behaviours</p>
                            </div>
                            <!--end box-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('after-scripts')
    <script>
        $(function() {
            $(".book-btn").on('click', function(e) {
                e.preventDefault();
                let pt_string = $(this).attr('id');
                let result = pt_string.split('_');
                let center = $('#location').val();
                let type = result[0];
                let pt_id = result[1];
                let route =
                    "{{ route('frontend.book.index', ['type' => '@type', 'check' => '@ptid', 'center' => '@centerId']) }}";
                route = route.replace('@type', type);
                route = route.replace('@ptid', pt_id);
                route = route.replace('@centerId', center);

                window.location.href = route;
            })
            $(".detail-btn").on('click', function(e) {
                e.preventDefault();
                let pt_string = $(this).attr('id');
                let result = pt_string.split('_');
                let center = $('#location').val();
                let type = result[0];
                let pt_id = result[1];
                let route =
                    "{{ route('frontend.book.index', ['type' => '@type', 'check' => '@ptid', 'center' => '@centerId']) }}";
                route = route.replace('@type', type);
                route = route.replace('@ptid', pt_id);
                route = route.replace('@centerId', center);

                window.location.href = route;
            })
        })

    </script>
@endpush