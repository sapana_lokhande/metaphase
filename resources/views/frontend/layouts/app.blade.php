<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage
    --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style(mix('css/frontend.css')) }}
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap"
        rel="stylesheet">
    <!-- CSS -->
    {{ style('v1/css/font-awesome.min.css') }}
    {{ style('v1/css/bootstrap.css') }}
    {{ style('v1/css/styles.css') }}
    {{ style('v1/css/responsive.css') }}
    {{ style('v1/css/owl.carousel.min.css') }}

    @stack('after-styles')
</head>

<body>
    {{ refreshPackages() }}
    @include('frontend.includes.header')
    <!--banner Image-->
    <div id="app">
        @include('includes.partials.messages')
        @yield('content')
    </div><!-- container -->
    <div class=" clearfix "></div>
    @include('frontend.includes.footer')
    <!-- JS -->
    {!! script('v1/js/jquery.js') !!}
    {!! script('v1/js/bootstrap.min.js') !!}
    {!! script('v1/js/owl.carousel.js',['defer' => 'defer']) !!}
    <script>
        /*menu*/
        $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass(" show ");
            }
            var $subMenu = $(this).next(" .dropdown-menu ");
            $subMenu.toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                $('.dropdown-submenu .show').removeClass(" show ");
            });

            return false;
        });

        /*banner*/
        var TxtType = function(el, toRotate, period) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 2000;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
        };

        TxtType.prototype.tick = function() {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];

            if (this.isDeleting) {
                this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
                this.txt = fullTxt.substring(0, this.txt.length + 1);
            }

            this.el.innerHTML = '<span class=" wrap ">' + this.txt + '</span>';

            var that = this;
            var delta = 200 - Math.random() * 100;

            if (this.isDeleting) {
                delta /= 2;
            }

            if (!this.isDeleting && this.txt === fullTxt) {
                delta = this.period;
                this.isDeleting = true;
            } else if (this.isDeleting && this.txt === '') {
                this.isDeleting = false;
                this.loopNum++;
                delta = 500;
            }

            setTimeout(function() {
                that.tick();
            }, delta);
        };

        window.onload = function() {
            var elements = document.getElementsByClassName('typewrite');
            for (var i = 0; i < elements.length; i++) {
                var toRotate = elements[i].getAttribute('data-type');
                var period = elements[i].getAttribute('data-period');
                if (toRotate) {
                    new TxtType(elements[i], JSON.parse(toRotate), period);
                }
            }
            // // INJECT CSS
            // var css = document.createElement(" style ");
            // css.type = " text/css ";
            // css.innerHTML = " .typewrite> .wrap { border-right: 0.08em solid transparent}";
            // document.body.appendChild(css);
        };

    </script>
    <script>
        $(document).ready(function() {
            // Default dropdown action to show/hide dropdown content
            $('.js-dropp-action').click(function(e) {
                e.preventDefault();
                $(this).toggleClass('js-open');
                $(this).parent().next('.dropp-body').toggleClass('js-open');
            });
            // Using as fake input select dropdown
            $('label').click(function() {
                $(this).addClass('js-open').siblings().removeClass('js-open');
                $('.dropp-body,.js-dropp-action').removeClass('js-open');
            });
            // get the value of checked input radio and display as dropp title
            $('input[name="dropp"]').change(function() {
                var value = $("input[name='dropp']:checked").val();
                $('.js-value').text(value);
            });
        });

    </script>
    <script type="text/javascript">
        /*price table slider*/
        $(document).ready(function() {
            if ($('.carousel1 .bbb_viewed_slider').length) {
                var viewedSlider = $('.carousel1 .bbb_viewed_slider');
                viewedSlider.owlCarousel({
                    loop: true,
                    margin: 30,
                    autoplay: true,
                    autoplayTimeout: 6000,
                    nav: false,
                    dots: false,
                    slidesToScroll: 1,
                    responsive: {
                        0: {
                            items: 1
                        },
                        575: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        991: {
                            items: 4
                        },
                        1199: {
                            items: 4
                        }
                    }
                });
                if ($('.carousel1 .bbb_viewed_prev').length) {
                    var prev = $('.carousel1 .bbb_viewed_prev');
                    prev.on('click', function() {
                        viewedSlider.trigger('prev.owl.carousel');
                    });
                }
                if ($('.carousel1 .bbb_viewed_next').length) {
                    var next = $('.carousel1 .bbb_viewed_next');
                    next.on('click', function() {
                        viewedSlider.trigger('next.owl.carousel');
                    });
                }
            }

            // second carousel
            if ($('.carousel2 .bbb_viewed_slider').length) {
                var viewedSlider2 = $('.carousel2 .bbb_viewed_slider');
                viewedSlider2.owlCarousel({
                    loop: true,
                    margin: 30,
                    autoplay: false,
                    autoplayTimeout: 6000,
                    nav: false,
                    dots: true,
                    responsive: {
                        0: {
                            items: 2
                        },
                        575: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        991: {
                            items: 6
                        },
                        1199: {
                            items: 6
                        }
                    }
                });
                if ($('.carousel2 .bbb_viewed_prev1').length) {
                    var prev = $('.carousel2 .bbb_viewed_prev1');
                    prev.on('click', function() {
                        viewedSlider2.trigger('prev.owl.carousel');
                    });
                }
                if ($('.carousel2 .bbb_viewed_next1').length) {
                    var next = $('.carousel2 .bbb_viewed_next1');
                    next.on('click', function() {
                        viewedSlider2.trigger('next.owl.carousel');
                    });
                }
            }
        });

    </script>

    @stack('before-scripts')
    
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/frontend.js')) !!}
    @stack('after-scripts')
</body>

</html>
