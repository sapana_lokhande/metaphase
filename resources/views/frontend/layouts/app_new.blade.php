<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage
    --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{-- {{ style(mix('css/frontend.css')) }} --}}
    {{ style('https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css') }}
    {{ style('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}
    {{ style('https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;500;600;700;800&display=swap') }}
    {{ style('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap') }}
    {{ style('css/style.css') }}
    {{ style('css/responsive.css') }}
    {{ style('https://unpkg.com/aos@2.3.1/dist/aos.css') }}
    @stack('after-styles')
</head>

<body>
    @include('includes.partials.read-only')

    <div id="app">
        @include('includes.partials.logged-in-as')
        {{-- @include('frontend.includes.nav') --}}
        @if(Auth::check())

            @if (Route::getCurrentRoute()->uri() == '/')
                 @include('frontend.includes.header')
            @else
                @include('frontend.includes.inner_header')
            @endif

        @else
        @include('frontend.includes.header')
        @endif
        <div>
            @include('includes.partials.messages')
            @yield('content')
        </div><!-- container -->
    </div><!-- #app -->

    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/frontend.js')) !!}
    @include('frontend.includes.footer')
    @stack('after-scripts')
    @include('includes.partials.ga')
</body>

</html>
