<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Laravel Boilerplate')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="v1/css/font-awesome.min.css" />
      <!-- CSS -->
      <link rel="stylesheet" href="v1/css/bootstrap.css">
      <link rel="stylesheet" href="v1/css/styles.css">
	  <link rel="stylesheet" href="v1/css/responsive.css">
      <link rel="stylesheet" href="v1/css/owl.carousel.min.css">
        @stack('after-styles')
    </head>
    <body>
        {{refreshPackages()}}
        @include('frontend.includes.header')
      <!--banner Image-->
      <div>
            @include('includes.partials.messages')
            @yield('content')
        </div><!-- container -->
      <div class=" clearfix "></div>
      <footer class=" footer-section ">
         <!-- Footer Elements -->
         <div class=" container ">
            <!-- Grid row-->
            <div class=" row ">
				<div class=" col-md-8 col-sm-6 col-xs-12 w-50-1">
					<h3>Metaphase Medical<br/> Dignostics private limited</h3>
					
                   <div class=" col-md-1 pl-0"><i class=" fa fa-phone "></i></div>
                  <div class=" col-md-11 ">
                    <a href=" mailto:+91 404 220 5116 ">+91 404 220 5116</a>
                  </div>
				  <div class="clearfix mb-20"></div>
                  <div class=" col-md-1 pl-0"><i class=" fa fa-envelope "></i></div>
                  <div class=" col-md-11 ">
                     <a href=" mailto:we@metaphase.in ">we@metaphase.in</a>
                  </div>
                 
               </div>
			  <div class="mob-5 col-md-4 col-sm-6 col-xs-12 w-50-1">
					<h3 class="mob-3">Operational Hours</h3>
					<div class="row">
                   <div class=" col-md-4 col-sm-4 col-xs-4">
						<h6>Weekdays</h6>
				   </div>
                  <div class="col-md-8 col-sm-8 col-xs-8">
                    <h6>6am - 11pm</h6>
                  </div>
				  </div>
				  <div class="mb-20"></div>
                  <div class="row">
                    <div class=" col-md-4 col-sm-4 col-xs-4">
						<h6>Sunday</h6>
				   </div>
                  <div class="col-md-8 col-sm-8 col-xs-8">
                    <h6>6am - 2pm</h6>
                  </div>
				  </div>
               </div>
			   </div>
			   
			   <div class="mb-50 clearfix"></div>
			  <div class="row">
               <!-- Grid column -->
               <div class=" col-md-3 col-sm-6 col-xs-12 w-50-1">
                  <h4>Services</h4>
                  <ul>
                     <li><a href=" # ">Covid test</a></li>
                     <li><a href=" # ">Home sample collection</a></li>
                     <li><a href=" # ">Radiology</a></li>
                     <li><a href=" # ">Pre natal support</a></li>
                     <li><a href=" # ">Cardiology</a></li>
                     <li><a href=" # ">TMT (Treadmill test)</a></li>
                     <li><a href=" # ">echo doppler</a></li>
                     <li><a href=" # ">ecg</a></li>
                     <li><a href=" # ">ultrasound scan 3d & 4d</a></li>
                     <li><a href=" # ">Bone Densitometry(BDM)</a></li>
                     <li><a href=" # ">Digital X-Ray</a></li>
                     <li><a href=" # ">Opthamology</a></li>
                  </ul>
               </div>
			    <div class=" col-md-3  col-sm-6 col-xs-12 w-50-1">
                  <h4>Clinical Support</h4>
                  <ul>
                     <li><a href=" # ">Pediatric Counselling</a></li>
                     <li><a href=" # ">Pre-natal Diagnostics Counselling</a></li>
                     <li><a href=" # ">Diet Counselling</a></li>
                     <li><a href=" # ">Psychiatric Counselling</a></li>
                     <li><a href=" # ">Depression Counselling</a></li>
                     <li><a href=" # ">Psychology Counselling</a></li>
                     <li><a href=" # ">Physiotherapy and Fitness Counselling</a></li>
                     <li><a href=" # ">Dementia Care</a></li>
                     <li><a href=" # ">Dentistry</a></li>
                     
                  </ul>
               </div>
               <div class=" col-md-3 col-sm-6 col-xs-12 w-50-1">
                  <h4>Diagnostics</h4>
                  <ul>
                     <li><a href=" # ">block 13</a></li>
                     <li><a href=" # ">New Born Baby Screening</a></li>
                     <li><a href=" # ">Pathology</a></li>
                     <li><a href=" # ">Biochemistry</a></li>
                     <li><a href=" # ">Serology and Microbiology</a></li>
                     <li><a href=" # ">Haematology</a></li>
                     <li><a href=" # ">Cytopathology</a></li>
                     <li><a href=" # ">Histopathology</a></li>
                     <li><a href=" # ">Molecular Diagnostic</a></li>
                    
                  </ul>
               </div>
               <!-- Grid column -->
               <div class=" col-md-3 col-sm-6 col-xs-12 w-50-1">
                  <h4>Follow Us</h4>
                  <div class=" socialicon ">
                     <ul>
                        <li><a href="#" target=" _blank "><i class="fa fa-facebook" class=" img-responsive "></i></a></li>
                        <li><a href="#" target=" _blank "><i class="fa fa-instagram" class=" img-responsive "></i></a></li>
                        <li><a href="#" target=" _blank "><i class="fa fa-twitter" class=" img-responsive "></i></a></li>
                     </ul>
                  </div>
                  <div class=" clearfix "></div>
                  <br>
               </div>
               
            </div>
            <!-- Grid row-->
         </div>
         <!-- Footer Elements -->
         <!-- Copyright -->
         <div class=" footer-copyright ">
			<div class="container">
				<div class="col-md-12">
					&copy; 2020, Metaphase. All Right Reservrd.
				</div>	
			</div>
			<div class="clearfix"></div>
         </div>
         <!-- Copyright -->
      </footer>
      <!-- JS -->
      <script src=" v1/js/jquery.js "></script>
      <script src=" v1/js/bootstrap.min.js "></script>
      <script src=" v1/js/owl.carousel.js "></script>
      <script>
         /*menu*/
         $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
                   if (!$(this).next().hasClass('show')) {
                       $(this).parents('.dropdown-menu').first().find('.show').removeClass(" show ");
                   }
                   var $subMenu = $(this).next(" .dropdown-menu ");
                   $subMenu.toggleClass('show');
         
                   $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                       $('.dropdown-submenu .show').removeClass(" show ");
                   });
         
                   return false;
               });
         
         /*banner*/
         var TxtType = function(el, toRotate, period) {
         this.toRotate = toRotate;
         this.el = el;
         this.loopNum = 0;
         this.period = parseInt(period, 10) || 2000;
         this.txt = '';
         this.tick();
         this.isDeleting = false;
         };
         
         TxtType.prototype.tick = function() {
         var i = this.loopNum % this.toRotate.length;
         var fullTxt = this.toRotate[i];
         
         if (this.isDeleting) {
         this.txt = fullTxt.substring(0, this.txt.length - 1);
         } else {
         this.txt = fullTxt.substring(0, this.txt.length + 1);
         }
         
         this.el.innerHTML = '<span class=" wrap ">'+this.txt+'</span>';
         
         var that = this;
         var delta = 200 - Math.random() * 100;
         
         if (this.isDeleting) { delta /= 2; }
         
         if (!this.isDeleting && this.txt === fullTxt) {
         delta = this.period;
         this.isDeleting = true;
         } else if (this.isDeleting && this.txt === '') {
         this.isDeleting = false;
         this.loopNum++;
         delta = 500;
         }
         
         setTimeout(function() {
         that.tick();
         }, delta);
         };
         
         window.onload = function() {
         var elements = document.getElementsByClassName('typewrite');
         for (var i=0; i<elements.length; i++) {
         var toRotate = elements[i].getAttribute('data-type');
         var period = elements[i].getAttribute('data-period');
         if (toRotate) {
         	new TxtType(elements[i], JSON.parse(toRotate), period);
         }
         }
         // INJECT CSS
         var css = document.createElement(" style ");
         css.type = " text/css ";
         css.innerHTML = " .typewrite> .wrap { border-right: 0.08em solid transparent}"; document.body.appendChild(css); }; 
      </script>
      <script>
         $(document).ready(function() {
         	// Default dropdown action to show/hide dropdown content
         	$('.js-dropp-action').click(function(e) {
         		e.preventDefault();
         		$(this).toggleClass('js-open');
         		$(this).parent().next('.dropp-body').toggleClass('js-open');
         	});
         	// Using as fake input select dropdown
         	$('label').click(function() {
         		$(this).addClass('js-open').siblings().removeClass('js-open');
         		$('.dropp-body,.js-dropp-action').removeClass('js-open');
         	});
         	// get the value of checked input radio and display as dropp title
         	$('input[name="dropp"]').change(function() {
         		var value = $("input[name='dropp']:checked").val();
         		$('.js-value').text(value);
         	});
         });
      </script>
      <script type="text/javascript">
         /*dropdown
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-36251023-1']);
         _gaq.push(['_setDomainName', 'jqueryscript.net']);
         _gaq.push(['_trackPageview']);
         (function() {
         	var ga = document.createElement('script');
         	ga.type = 'text/javascript';
         	ga.async = true;
         	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         	var s = document.getElementsByTagName('script')[0];
         	s.parentNode.insertBefore(ga, s);
         })();
         */
		 
         /*price table slider*/
         $(document).ready(function() {
         if($('.carousel1 .bbb_viewed_slider').length) {
			 var viewedSlider = $('.carousel1 .bbb_viewed_slider');
			 viewedSlider.owlCarousel({
				 loop: true,
				 margin: 30,
				 autoplay: true,
				 autoplayTimeout: 6000,
				 nav: false,
				 dots: false,
				 slidesToScroll: 1,
				 responsive: {
				 0: {
				 items: 1
				 },
				 575: {
				 items: 2
				 },
				 768: {
				 items: 3
				 },
				 991: {
				 items: 4
				 },
				 1199: {
				 items: 4
				 }
			 }
			});
				if($('.carousel1 .bbb_viewed_prev').length) {
					var prev = $('.carousel1 .bbb_viewed_prev');
					prev.on('click', function() {
						viewedSlider.trigger('prev.owl.carousel');
					});
				}
				if($('.carousel1 .bbb_viewed_next').length) {
					var next = $('.carousel1 .bbb_viewed_next');
					next.on('click', function() {
						viewedSlider.trigger('next.owl.carousel');
					});
				}
			}
			
			// second carousel
			if($('.carousel2 .bbb_viewed_slider').length) {
			 var viewedSlider2 = $('.carousel2 .bbb_viewed_slider');
			 viewedSlider2.owlCarousel({
				 loop: true,
				 margin: 30,
				 autoplay: false,
				 autoplayTimeout: 6000,
				 nav: false,
				 dots: true,
				 slidesToScroll: 1,
				 responsive: {
				 0: {
				 items: 2
				 },
				 575: {
				 items: 2
				 },
				 768: {
				 items: 3
				 },
				 991: {
				 items: 6
				 },
				 1199: {
				 items: 6
				 }
			 }
			});
				if($('.carousel2 .bbb_viewed_prev1').length) {
					var prev = $('.carousel2 .bbb_viewed_prev1');
					prev.on('click', function() {
						viewedSlider2.trigger('prev.owl.carousel');
					});
				}
				if($('.carousel2 .bbb_viewed_next1').length) {
					var next = $('.carousel2 .bbb_viewed_next1');
					next.on('click', function() {
						viewedSlider2.trigger('next.owl.carousel');
					});
				}
			}
         });
      </script>

     
    </body>
</html>

