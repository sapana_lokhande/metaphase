@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
	<div class="contact-page">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <div class="col-md-6 col-sm-5 img-anim">
                    <img src="v1/images/banner/contactpage-banner-image.jpg" alt="wrapkit" class="img-fluid" />
                </div>
                <!-- Column -->
                <div class="col-md-6 col-sm-7 ml-auto align-self-top mt-8">
					<div class="plr mob-plr">
					<h1 class="title animated fadeInRight">Get In Touch</h1>
					<div class="row clearfix margin-5 mob-30">
						<div class="col-md-2 col-sm-5 col-xs-2">
							<img src="v1/images/phone-icon.png" class="mob-img" />
						</div><!--end col-md-2-->
						<div class="col-md-10 col-sm-7 col-xs-10">
							<h3>+91 6309 738222</h3>
						</div><!--end col-md-10-->
					</div><!--end phone row-->
					<div class="row clearfix margin-5 mob-30">
						<div class="col-md-2 col-sm-5 col-xs-2">
							<img src="v1/images/email-icon.png" class="mob-img" />
						</div><!--end col-md-2-->
						<div class="col-md-10 col-sm-7 col-xs-10">
							<h3>We@metaphase.in</h3>
						</div><!--end col-md-10-->
					</div><!--end email row-->
					<div class="row clearfix margin-5 mob-30">
						<div class="col-md-2 col-sm-5 col-xs-2">
							<img src="v1/images/map-icon.png" class="mob-img" />
						</div><!--end col-md-2-->
						<div class="col-md-10 col-sm-7 col-xs-10">
							<p>6-3-1090, Pancom Chambers, Flat  No.12, 1st Floor, Rajbhavan Road, Somajiguda, Hyderabad - 500082</p>
							<p>Survey No. 157/8, Plot No. 1, A.P. Textbook Colony, Karkhana, Gunrock, Thokatta Village, Secunderabad, Telengana 500009</p>
						</div><!--end col-md-10-->
					</div><!--end location row-->
					</div>
                </div>
                <!-- Column -->
            </div>
        
    <div class="row">
        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 align-self-center">
		<h1 class="mb-3 title animated fadeInRight">@lang('labels.frontend.contact.box_title')</h1>
            <div class="card-body p-0">
                    {{ html()->form('POST', route('frontend.contact.send'))->open() }}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
									<div class="col-md-3 col-sm-4 pl-0">
										{{ html()->label(__('validation.attributes.frontend.name'))->for('name') }}
									</div><!--end col-md-3-->
									<div class="col-md-5 col-sm-5 p-0">
										{{ html()->text('name', optional(auth()->user())->name)
                                        ->class('form-control')
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
									</div><!--end col-md-5-->
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
									<div class="col-md-3 col-sm-4 pl-0">
										{{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}
									</div><!--end col-md-3-->
									<div class="col-md-5 col-sm-5 p-0">
                                    {{ html()->email('email', optional(auth()->user())->email)
                                        ->class('form-control')
                                        ->attribute('maxlength', 191)
                                        ->required() }}
									</div><!--end col-md-5-->
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
									<div class="col-md-3 col-sm-4 pl-0">
										{{ html()->label(__('validation.attributes.frontend.phone'))->for('phone') }}
									</div><!--end col-md-3-->
									<div class="col-md-5 col-sm-5 p-0">
                                    {{ html()->text('phone')
                                        ->class('form-control')
                                        ->attribute('maxlength', 191)
                                        ->required() }}
									</div><!--end col-md-5-->
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
									<div class="col-md-3 col-sm-4 pl-0">
										{{ html()->label(__('validation.attributes.frontend.message'))->for('message') }}
									</div><!--end col-md-3-->
									<div class="col-md-5 col-sm-5 pl-0">
                                    {{ html()->textarea('message')
                                        ->class('form-control')
                                        ->attribute('rows', 3)
                                        ->required() }}
									</div><!--end col-md-5-->
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        @if(config('access.captcha.contact'))
                            <div class="row">
                                <div class="col">
                                    @captcha
                                    {{ html()->hidden('captcha_status', 'true') }}
                                </div><!--col-->
                            </div><!--row-->
                        @endif

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{ form_submit(__('labels.frontend.contact.button')) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                </div><!--card-body-->
			</div><!--col-->
		</div><!--row-->
	</div>
    </div>
@endsection

@push('after-scripts')
    @if(config('access.captcha.contact'))
        @captchaScripts
    @endif
@endpush