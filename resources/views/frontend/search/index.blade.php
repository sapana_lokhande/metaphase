@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="static-slider4 pb-0">
        <div class="container">
            <!-- Row  -->
            <div class="row">
                <div class="col-md-12 col-sm-12 img-anim">
                    <img src="v1/images/banner/packagelist-banner-image.jpg" alt="wrapkit" class="img-fluid">
                </div>
                <!-- Column -->
            </div>
        </div>
    </div>
    <section class="price-section" style="background:#fff; padding-bottom:20px">
        <div class=" container ">
            <div class=" col-md-12 ">
                <h1>Your Search Results</h1>
                <div class="clearfix"></div>

                <div class="row">
                    @php
                    $centerId = request()->input('location')??null;
                    @endphp
                    @foreach ($result['packageResult'] as $package)
                        <div class="mb-3 text-center col-md-3 ipad-col-4">
                            <div class=" block block-pricing  mb-4 box-shadow">
                                <h6 class="category1">{{ $package['testName'] }}</h6>

                                <div class="card-body p-0">
                                    @php
                                    $tests = array_slice($package['testList'],0,3);
                                    // dd($tests);
                                    @endphp
                                    <ul class=" list-ul">
                                        @foreach ($tests as $test)
                                            <li>{{ $test['testName'] }}</li>
                                        @endforeach
                                    </ul>
                                    <h1 class=" block-caption "><i class=" fa fa-rupee "></i> {{ $package['testAmount'] }}</h1>
                                    {{-- <a class="btn-round1 btn"
                                        href="{{ route('frontend.book.details', ['type' => 'package', 'check' => $package['testID'], 'center' => $centerId]) }}">View
                                        Details</a> --}}
										
                                    <button type="button" id="package_{{ $package['testID'] }}"
                                        class="btn  btn-danger btn-round  btn-lg btn-block btn-outline-primary book-btn">TEST
                                        NOW</button>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>

    @if (!empty($result['testResult']))
    <section class="price-section" style="background:#fff; padding-top:0">
        <div class=" container ">
            <div class=" col-md-12 ">
                <h1>Test list</h1>
                <div class="clearfix"></div>

                <div class="row">

                    @foreach ($result['testResult'] as $test)
                        <div class="mb-3 text-center col-md-3 ipad-col-4">
                            <div class="block block-pricing mb-4 box-shadow">
                                <h6 class="category1">{{ $test['testName'] }}</h6>

                                <div class="card-body p-0">
                                    <ul class=" list-ul">
                                        <li>{{ $test['testName'] }}</li>
                                    </ul>
                                    <h1 class=" block-caption "><i class=" fa fa-rupee "></i> {{ $test['testAmount'] }}</h1>
                                    <button type="button" id="test_{{ $test['testID'] }}"
                                        class="btn btn-danger btn-round btn-lg btn-block btn-outline-primary book-btn ">TEST
                                        NOW</button>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>        
    @endif

@endsection

@push('after-scripts')
    <script>
        $(function() {
            $(".book-btn").on('click', function() {
                let pt_string = $(this).attr('id');
                let result = pt_string.split('_');
                let type = result[0];
                let pt_id = result[1];
                let route =
                    "{{ route('frontend.book.index', ['type' => '@type', 'check' => '@ptid', 'center' => $centerId]) }}";
                route = route.replace('@type', type);
                route = route.replace('@ptid', pt_id);

                window.location.href = route;
            })
        })

    </script>
@endpush
