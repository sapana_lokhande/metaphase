@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.contact.management'))

@section('breadcrumb-links')
   
@endsection

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.contact.contact') }}
                        <small class="text-muted">{{ __('labels.backend.contact.active') }}</small>
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
               
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="contact-table">
                            <thead>
                            <tr>
                                <th>@lang('labels.backend.contact.table.id')</th>
                                <th>@lang('labels.backend.contact.table.name')</th>
                                <th>@lang('labels.backend.contact.table.email')</th>
                                <th>@lang('labels.backend.contact.table.phone')</th>
                                <th>@lang('labels.backend.contact.table.message')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.21/api/processing().js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    
    <script>
        $(function () {
            
            $('#contact-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.contact.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }   
                },
                columns: [
                    {data: 'id', name: '{{config('table.contact_us')}}.id'},
                    {data: 'name', name: '{{config('table.contact_us')}}.name'},
                    {data: 'email', name: '{{config('table.contact_us')}}.email'},
                    {data: 'phone', name: '{{config('table.contact_us')}}.phone'},
                    {data: 'message', name: '{{config('table.contact_us')}}.message'},
                ],
                "columnDefs": [
                    {
                        "targets": [4],
                        "className": "btn-td"
                    }
                ],
                "language": {
                    processing: '<div class="spinner-grow" style="width: 3rem; height: 3rem;border: none" role="status">\n' +

                        '</div> '
                },
                createdRow: function (row) {
                    try {
                        processDeleteBtn(row);
                    } catch (e) {

                    }

                },
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
     
@endpush
