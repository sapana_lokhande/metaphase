@extends('backend.layouts.app')

@section ('title', trans('labels.backend.package.management') . ' | ' . trans('labels.backend.package.create'))


@section('breadcrumb-links')
    @include('backend.package.includes.partials.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.package.store'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            @lang('labels.backend.package.management')
                            <small class="text-muted">@lang('labels.backend.package.create')</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr>

                <div class="row mt-4 mb-4">
                    <div class="col">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.package.package_id'))->class('col-md-2 form-control-label')->for('title') }}

                            <div class="col-md-10">
                                {{ html()->text('package_id')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.package.package_id'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.package.image'))->class('col-md-2 form-control-label')->for('image') }}

                            <div class="col-md-10">
                            {{ html()->file('image')
                            ->class('form-control')
                            ->id('image')
                            ->required() }} 
                            </div><!--col-->
                            
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.package.active'))->class('col-md-2 form-control-label')->for('email') }}
 
                            <div class="col-md-10">
                            {{ html()->checkbox('active', '1', isset($package) && $pages->active == 1)->value(1) }}
                            </div><!--col-->
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.package.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
    
@endsection
