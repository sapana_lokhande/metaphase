@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.package.management'))

@section('breadcrumb-links')
    @include('backend.package.includes.partials.breadcrumb-links')
@endsection

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.package.package') }}
                        <small class="text-muted">{{ __('labels.backend.package.active') }}</small>
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                @include('backend.package.includes.partials.package-header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="package-table">
                            <thead>
                            <tr>
                                <th>@lang('labels.backend.package.table.id')</th>
                                <th>@lang('labels.backend.package.table.package_id')</th>
                                <th>@lang('labels.backend.package.table.image')</th>
                                <th>@lang('labels.backend.package.table.created_at')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.21/api/processing().js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    
    <script>
        $(function () {

              /*Display date format to mm-dd-yyyy */
              function formatted_date(d)
            {
                return moment(d).format("MM-DD-YYYY HH:mm:ss");
            }

            function imageurl(img)
            {
                return '<img src="/storage/package/'+img+'" height="50px">';
            }

            
            $('#package-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.package.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }   
                },
                columns: [
                    {data: 'id', name: '{{config('table.package_images')}}.id'},
                    {data: 'package_id', name: '{{config('table.package_images')}}.package_id'},
                    {data: 'image', name: '{{config('table.package_images')}}.image',render:imageurl},
                    {data: 'created_at', name: '{{config('table.package_images')}}.created_at',render:formatted_date},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                "columnDefs": [
                    {
                        "targets": [4],
                        "className": "btn-td"
                    }
                ],
                "language": {
                    processing: '<div class="spinner-grow" style="width: 3rem; height: 3rem;border: none" role="status">\n' +

                        '</div> '
                },
                createdRow: function (row) {
                    try {
                        processDeleteBtn(row);
                    } catch (e) {

                    }

                },
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
     
@endpush
