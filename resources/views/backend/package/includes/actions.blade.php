    <div class="btn-group" role="group" aria-label="@lang('labels.backend.access.users.user_actions')">
        
        <a href="{{ route('admin.package.edit', $package) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.edit')" class="btn btn-primary">
            <i class="fas fa-edit"></i>
        </a>
    </div>

   
