@extends('backend.layouts.app')

@section('title', __('labels.backend.package.management') . ' | ' . __('labels.backend.package.edit'))

@section('breadcrumb-links')
    @include('backend.package.includes.partials.breadcrumb-links')
@endsection

@section('content')
    {{ html()->modelForm($package, 'PATCH', route('admin.package.update', $package->id))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.package.management')
                        <small class="text-muted">@lang('labels.backend.package.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.package.package_id'))->class('col-md-2 form-control-label')->for('first_name') }}
                    <input type="hidden" name="old_img" id="old_img" value="{{ $package->image }}">
                        <div class="col-md-10">
                            {{ html()->text('package_id')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.package.package_id'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                   
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.package.image'))->class('col-md-2 form-control-label')->for('image') }}

                        <div class="col-md-10">
                        {{-- {{ html()->file('image')
                        ->class('form-control')
                        ->id('image')
                        ->required() }}  --}}
                        <div class="col-md-5">
                            <img src="{{ url('storage/package').'/'.($package->image) }}" style="height:200px;">&nbsp;
                        </div>
                        <div class="custom-file">
                            {{ html()->file('image')->class('form-control')->accept('image/jpeg, image/jpg, image/png')->autofocus() }}
                             {{-- {{ html()->label('Choose file')->class('custom-file-label')->for('image')}} --}}
                            <p></p>
                        </div>
                        </div><!--col-->
                        
                    </div><!--form-group-->
                    <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.package.active'))->class('col-md-2 form-control-label')->for('email') }}
 
                            <div class="col-md-10">
                                {{ html()->hidden('active')->value(0)}}
                            @if($package->active == 1)
                            
                                {{ html()->checkbox('active', '1', isset($package) && $package->active == 1)->value(1) }}
                            @else
                                {{ html()->checkbox('active', '0', isset($package) && $package->active == 0)->value(1) }}
                            @endif
                            
                            </div><!--col-->
                    </div><!--form-group-->
                   
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.package.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
