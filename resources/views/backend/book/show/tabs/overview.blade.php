@php
$packageName = $packageInfo['testName']??null;
$packagePrice = $packageInfo['testAmount']??null;
$testList = $packageInfo['testList']??null;
@endphp

<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.package_id')</th>
                <td>{{ $book->package_id }} ({{ $packageName }})</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.price')</th>
                <td>&#8377; {{ $packagePrice }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.lab_location')</th>
                <td>{{ $book->center->location }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.csname')</th>
                <td>{{ $book->customer_name }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.csemail')</th>
                <td>{{ $book->customer_email }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.csphone')</th>
                <td>{{ $book->customer_phone }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.csnote')</th>
                <td>{{ $book->customer_note }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.bookdate')</th>
                <td>
                    @if (!empty($book->booking_date))
                        {{ formatDate($book->booking_date) }}
                    @endif

                </td>
            </tr>

            <tr>
                <th>@lang('labels.backend.book.tabs.content.overview.created_at')</th>
                <td>{{ $book->created_at->format('j M Y, g:i A') }}</td>
            </tr>

            @if ($book->type == 'package')
                <tr>
                    <th>@lang('labels.backend.book.tabs.content.overview.testincluded')</th>
                    <td>
                        <ul class="list-group">
                            @foreach ($testList as $test)
                                <li class="list-group-item">{{ $test['testID'] }} - ({{ $test['testName'] }}) &#8377;
                                    {{ $test['testAmount'] }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endif
        </table>
    </div>
</div>
<!--table-responsive-->

@push('after-styles')
    <style>
        .list-group {
            max-height: 300px;
            margin-bottom: 10px;
            overflow: scroll;
            -webkit-overflow-scrolling: touch;
        }

    </style>
@endpush
