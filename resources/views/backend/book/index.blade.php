@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.book.management'))
{{-- 
@section('breadcrumb-links')
    @include('backend.book.includes.partials.breadcrumb-links')
@endsection --}}

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.book.management') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                @include('backend.book.includes.partials.book-header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="book-table">
                            <thead>
                            <tr>
                                <th>@lang('labels.backend.book.table.package_id')</th>
                                <th>@lang('labels.backend.book.table.lab_location')</th>
                                <th>@lang('labels.backend.book.table.csname')</th>
                                <th>@lang('labels.backend.book.table.csemail')</th>
                                <th>@lang('labels.backend.book.table.created_at')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.21/api/processing().js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    
    <script>
        $(function () {

 
            function imageurl(img)
            {
                return '<img src="/storage/book/'+img+'" height="50px">';
            }

            
            $('#book-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.book.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false},
                    error: function (xhr, err) {
                      
                    }   
                },
                columns: [
                    {data: 'package_id', name: '{{config('table.bookedpt')}}.package_id'},
                    {data: 'center_id', name: '{{config('table.bookedpt')}}.center_id'},
                    {data: 'customer_name', name: '{{config('table.bookedpt')}}.customer_name'},
                    {data: 'customer_email', name: '{{config('table.bookedpt')}}.customer_email'},
                    {data: 'created_at', name: '{{config('table.bookedpt')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                "columnDefs": [
                    {
                        "targets": [5],
                        "className": "btn-td"
                    }
                ],
                "language": {
                    processing: '<div class="spinner-grow" style="width: 3rem; height: 3rem;border: none" role="status">\n' +

                        '</div> '
                },
                createdRow: function (row) {
                    try {
                        processDeleteBtn(row);
                    } catch (e) {

                    }

                },
                order: [[4, "desc"]],
                searchDelay: 500
            });
        });
    </script>
     
@endpush
