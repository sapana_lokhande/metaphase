@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.deactivated'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.users.management')
                    <small class="text-muted">@lang('labels.backend.access.users.deactivated')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.users.table.last_name')</th>
                            <th>@lang('labels.backend.access.users.table.first_name')</th>
                            <th>@lang('labels.backend.access.users.table.email')</th>
                            <th>@lang('labels.backend.access.users.table.confirmed')</th>
                            <th>@lang('labels.backend.access.users.table.roles')</th>
                            <th>@lang('labels.backend.access.users.table.last_updated')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.21/api/processing().js"></script>
    <script>

        $(function () {
            $('#users-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                searchDelay: 350,
                ajax: {
                    url: '{{ route("admin.auth.user.get") }}',
                    type: 'post',
                    data: {status: 0, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'last_name', name: '{{config('tables.users')}}.last_name'},
                    {data: 'first_name', name: '{{config('tables.users')}}.first_name'},
                    {data: 'email', name: '{{config('tables.users')}}.email'},
                    {data: 'confirmed', name: '{{config('tables.users')}}.confirmed'},
                    {data: 'roles', name: 'roles', searchable: false, sortable: false},
                    {data: 'last_updated', name: 'updated_at', searchable: false},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false},

                ],
                "columnDefs": [
                    {
                        "targets": [6],
                        "className": "btn-td"
                    }
                ],
                "processing": true,
                "language": {
                    processing: '<div class="spinner-grow" style="width: 3rem; height: 3rem;border: none" role="status">\n' +

                        '</div> '
                },
                createdRow: function (row) {
                    try {
                        processDeleteBtn(row);
                    } catch (e) {

                    }

                },
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endpush
