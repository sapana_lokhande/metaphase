<?php
    use App\Models\CmsPage\CmsPage;


    function get_cms_block($slug)
    {
        $cms = getCmsBySlug($slug);
        if(!empty($cms)) {
            if($cms->body != null) {
                return $cms->body;
            }
        }
    }

    /**
     * get Cms By slug 
     *
     * @param  $input
     * @param  $cms
     *
     * @throws GeneralException
     */
    function getCmsBySlug($slug)
    {
        return CmsPage::where('slug', $slug)->active(1)->first();
    }
?>