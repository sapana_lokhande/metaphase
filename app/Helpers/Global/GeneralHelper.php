<?php

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('home_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
        if (auth()->check()) {
            if (auth()->user()->can('view backend')) {
                return 'admin.dashboard';
            }

            return 'frontend.user.dashboard';
        }

        return 'frontend.index';
    }
}

if (! function_exists('getMasterAttributeOptionName')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function getMasterAttributeOptionName($data, $string=false)
    {
        $userData = [];
        if(isJson($data->pivot->value))
        {
            $userData =  json_decode($data->pivot->value,true);
        }

        if($userData){
            $result = App\Models\Master\AttributesOption\MasterAttributesOption::whereIn('id',$userData)->get()->pluck('value')->toArray();
            if($string)
            {
                $result = implode(", ",$result);
            }
            return $result;
        } else {
            $result = App\Models\Master\AttributesOption\MasterAttributesOption::find($data->pivot->value);
            return $result->value??null;
        }
        
        
    }
}

if (! function_exists('isJson')) {
    function isJson($str) {
        $json = json_decode($str);
        return $json && $str != $json;
    }
}

if (! function_exists('jsonToArray')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function jsonToArray($data)
    {
        return json_decode($data,true);
        
    }
}

if (! function_exists('refreshPackages')) {
   
    function refreshPackages()
    {
        // $packagesRepository = new App\Repositories\Backend\Packages\PackagesRepository();

        // $packagesRepository->refreshPackages('sdsf');
    }
}

if (!function_exists('formatDate')) {

    function formatDate($date_string = null)
    {
        try {
            if (empty($date_string)) {
                return null;
            }
            $date = \Carbon\Carbon::parse($date_string);
            return $date->format('j M Y, g:i A');
        } catch (\Exception $e) {
            return null;
        }
    }
}