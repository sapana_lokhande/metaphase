<?php

namespace App\Http\Controllers\Frontend\Search;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Search\SearchRepository;
use App\Http\Requests\Frontend\Search\SearchPackageTestRequest;

/**
 * Class SearchController.
 */
class SearchController extends Controller
{

    /**
     * @var SearchRepository
     */
    protected $SearchRepository;

    /**
     * ProfileController constructor.
     *
     * @param SearchRepository $SearchRepository
     */
    public function __construct(SearchRepository $SearchRepository)
    {
        $this->SearchRepository = $SearchRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SearchPackageTestRequest $request)
    {
        $result = $this->SearchRepository->searchPackagesTests($request->all());
        return view('frontend.search.index')
            ->withResult($result);
    }
}
