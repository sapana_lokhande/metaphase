<?php

namespace App\Http\Controllers\Frontend\Book;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Book\BookRepository;
use App\Repositories\Frontend\Search\SearchRepository;
use App\Http\Requests\Frontend\Book\BookPackageTestRequest;

/**
 * Class BookController.
 */
class BookController extends Controller
{

    /**
     * @var BookRepository
     */
    protected $bookRepository;

    protected $searchRepository;

    /**
     * BookController constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(SearchRepository $searchRepository, BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
        $this->searchRepository = $searchRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($type, $center, $check)
    {
        $packageInfo = $this->searchRepository->getPackagesTests($type, $center, $check);
        return view('frontend.book.index')
            ->withPackageInfo($packageInfo)
            ->withType($type)
            ->withCenter($center)
            ->withCheck($check);
    }

    /**
     * 
     */
    public function bookPT(BookPackageTestRequest $request)
    {
        $response = $this->bookRepository->bookTest($request->all());
        $result_response = [
            'status' => $response !== false ? 'OK' : "ERROR",
            'message' => $response !== false ? 'Test booked successfully' : "Unable to process request"
        ];
        return response()->json($result_response, $response !== false ? 200 : 422);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($type, $center, $check)
    {
        $packageInfo = $this->searchRepository->getPackagesTests($type, $center, $check);
        return view('frontend.book.details')
            ->withPackageInfo($packageInfo)
            ->withType($type)
            ->withCenter($center)
            ->withCheck($check);
    }
}
