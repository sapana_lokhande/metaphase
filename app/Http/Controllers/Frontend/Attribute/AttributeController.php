<?php

namespace App\Http\Controllers\Frontend\Attribute;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Attributes\MasterAttributesRepository;
use Illuminate\Http\Request;

/**
 * Class ContactController.
 */
class AttributeController extends Controller
{
    /**
     * @var $masterAttributeRepository
     */
    protected $masterAttributeRepository;

    public function __construct(MasterAttributesRepository $masterAttributeRepository)
    {
        $this->masterAttributeRepository =$masterAttributeRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

    }

    public function getAttribute(Request $request)
    {
        $attribute = $this->masterAttributeRepository->getAttributeWithOptions($request->all());
        return response()->json($attribute);
    }

    public function getAllAttribute(Request $request)
    {
        $attribute = $this->masterAttributeRepository->getAllAttributeWithOptions($request->all());
        return response()->json($attribute);
    }
}
