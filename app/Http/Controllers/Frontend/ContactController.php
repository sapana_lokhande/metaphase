<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Contact\SendContactRequest;
use App\Mail\Frontend\Contact\SendContact;
use Illuminate\Support\Facades\Mail;
use App\Models\Contact\Contact;
use App\Repositories\Frontend\Contact\ContactRepository;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{
    public function __construct(ContactRepository $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.contact');
    }

    /**
     * @param SendContactRequest $request
     *
     * @return mixed
     */
    public function send(SendContactRequest $request)
    {
        Mail::send(new SendContact($request));

        $this->contact->create($request->only(
            'name', 
            'email', 
            'phone',
            'message'
        ));

        return redirect()->back()->withFlashSuccess(__('alerts.frontend.contact.sent'));
    }
}
