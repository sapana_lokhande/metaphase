<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\LabCenters\LabCenters;
use App\Repositories\Backend\Packages\PackagesRepository;
use App\Repositories\Frontend\Search\SearchRepository;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @var PackagesRepository
     */
    protected $packagesRepository;

    /**
     * ConfirmAccountController constructor.
     *
     * @param PackagesRepository $packagesRepository
     */
    public function __construct(PackagesRepository $packagesRepository, SearchRepository $searchRepository)
    {
        $this->packagesRepository = $packagesRepository;
        $this->searchRepository = $searchRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $labCenters = LabCenters::Active()->pluck('location','id');
        $packagesTests = $this->packagesRepository->getPackages();
        $randomPackages = $this->searchRepository->getPackageSlider($labCenters);
        return view('frontend.index')
            ->withPackage($packagesTests)
            ->withPackages($randomPackages)
            ->withLabCenters($labCenters);
    }

    public function howItWorks()
    {
        return view('frontend.howitworks');
    }

    public function healthPackages()
    {
        return view('frontend.package.index');
    }
}
