<?php
namespace App\Http\Controllers\Backend\Contact;

use App\Http\Controllers\Controller;
use DataTables;
use App\Repositories\Frontend\Contact\ContactRepository;
use App\Http\Requests\Frontend\Contact\ManageRequest;
/**
 * Class ContactTableController.
 */
class ContactTableController extends Controller
{
    /**
     * @var ContactRepository
     */
    protected $repository;

    /**
     * ContactController constructor.
     *
     * @param ContactRepository $repository
     */
    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageRequest $request)
    {
        // dd($request);
        return Datatables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
        ->escapeColumns(['id'])
        ->make(true);
       
    }
}
