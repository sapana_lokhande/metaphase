<?php namespace App\Http\Controllers\Backend\Contact;

/**
 * Class ContactController
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Http\Request;
use App\Models\Package\Package;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Frontend\Contact\ManageRequest;
use App\Repositories\Frontend\Contact\ContactRepository;

class ContactController extends Controller
{
    /**
     * ContactRepository
     *
     * @var object
     */
    protected $repository;

    
    /**
     * ContactController constructor.
     *
     * @param ContactRepository $repository
     */
    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Index 
     *
     * @param ManageRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageRequest $request)
    {
        return view('backend.contact.index');
        // ->withHeroBanners($this->repository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Create 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function create(ManageRequest $request)
    {
        return view('backend.package.create');
    }

    
}
