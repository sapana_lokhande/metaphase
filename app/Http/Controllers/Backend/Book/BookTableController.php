<?php

namespace App\Http\Controllers\Backend\Book;

use DataTables;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Book\ManageRequest;
use App\Repositories\Backend\Book\BookRepository;
use App\Repositories\Backend\Search\SearchRepository;

/**
 * Class BookTableController.
 */
class BookTableController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $repository;
    protected $searchRepository;

    /**
     * BookController constructor.
     *
     * @param BookRepository $repository
     */
    public function __construct(BookRepository $repository, SearchRepository $searchRepository)
    {
        $this->repository = $repository;
        $this->searchRepository = $searchRepository;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageRequest $request)
    {
        // dd($request);
        return Datatables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->escapeColumns(['book_id'])
            ->editColumn('package_id', function ($book) {
                $packageInfo = $this->getPackageInfo($book);
                return $book->package_id . '<br>' . $packageInfo['testName'];
            })
            ->editColumn('center_id', function ($book) {
                return $book->center->location;
            })
            ->editColumn('created_at', function ($book) {
                return $book->created_at->format('j M Y, g:i A');
            })
            ->editColumn('actions', function ($book) {
                return view('backend.book.includes.actions', ['book' => $book]);
            })
            ->make(true);
    }

    private function getPackageInfo($book)
    {
        try {
            $result = $this->searchRepository->getPackagesTests($book->type, $book->center_id, $book->package_id);
            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }
}
