<?php

namespace App\Http\Controllers\Backend\Book;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Book\BookRepository;
use App\Http\Requests\Backend\Book\ManageBookRequest;
use App\Repositories\Backend\Search\SearchRepository;
use App\Http\Requests\Backend\Book\BookPackageTestRequest;
use App\Models\Book\BookTests;

/**
 * Class BookController.
 */
class BookController extends Controller
{

    /**
     * @var BookRepository
     */
    protected $bookRepository;

    protected $searchRepository;

    /**
     * BookController constructor.
     *
     * @param BookRepository $bookRepository
     */
    public function __construct(SearchRepository $searchRepository, BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
        $this->searchRepository = $searchRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.book.index');
    }

    public function show(ManageBookRequest $request, BookTests $book)
    {
        $packageInfo = $this->searchRepository->getPackagesTests($book->type, $book->center_id, $book->package_id);
        return view('backend.book.show')
            ->withBook($book)
            ->withPackageInfo($packageInfo);
    }


    public function destroy(ManageBookRequest $request, BookTests $book)
    {
        $this->bookRepository->deleteById($book->id);

        return redirect()->route('admin.book.index')->withFlashSuccess(__('alerts.backend.book.deleted'));
    }
}
