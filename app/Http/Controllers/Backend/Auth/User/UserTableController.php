<?php
namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use DataTables;
use App\Repositories\Backend\Auth\UserRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
/**
 * Class UserTableController.
 */
class UserTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageUserRequest $request)
    {
        return Datatables::of($this->userRepository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->escapeColumns(['first_name', 'last_name', 'email'])
            ->editColumn('confirmed', function ($user) {
                return view('backend.auth.user.includes.confirm', ['user' => $user]);
            })
        ->editColumn('roles', function ($user) {
            return $user->roles_label;
        })
        ->editColumn('last_updated', function ($user) {
            return $user->updated_at->diffForHumans();
        })
        ->editColumn('actions', function ($user) {
            return view('backend.auth.user.includes.actions', ['user' => $user]);
        })

            ->make(true);
    }
}
