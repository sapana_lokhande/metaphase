<?php
namespace App\Http\Controllers\Backend\Package;

use App\Http\Controllers\Controller;
use DataTables;
use App\Repositories\Backend\Package\PackageRepository;
use App\Http\Requests\Backend\Package\ManageRequest;
/**
 * Class PackageTableController.
 */
class PackageTableController extends Controller
{
    /**
     * @var PackageRepository
     */
    protected $repository;

    /**
     * PackageController constructor.
     *
     * @param PackageRepository $repository
     */
    public function __construct(PackageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageRequest $request)
    {
        // dd($request);
        return Datatables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
        ->escapeColumns(['package_id'])
        ->editColumn('actions', function ($package) 
        {
            return view('backend.package.includes.actions', ['package' => $package]);
        })
        ->make(true);
       
    }
}
