<?php namespace App\Http\Controllers\Backend\Package;

/**
 * Class PackageController
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Http\Request;
use App\Models\Package\Package;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Package\StoreRequest;
use App\Http\Requests\Backend\Package\ManageRequest;
use App\Http\Requests\Backend\Package\UpdateRequest;
use App\Repositories\Backend\Package\PackageRepository;

class PackageController extends Controller
{
    /**
     * PackageRepository
     *
     * @var object
     */
    protected $repository;

    
    /**
     * HeroBannersController constructor.
     *
     * @param PackageRepository $repository
     */
    public function __construct(PackageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Index 
     *
     * @param ManageRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageRequest $request)
    {
        return view('backend.package.index');
        // ->withHeroBanners($this->repository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Create 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function create(ManageRequest $request)
    {
        return view('backend.package.create');
    }

    /**
     * Store 
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpg,jpeg,png|max:4096',
            ],
            $messages = [
                'mimes' => 'Only Image files are allowed (jpg, jpeg, png).'
            ]
        );

        $this->repository->create($request->only(
            'package_id', 
            'image',
            'active'
        ));
       
        return redirect()->route('admin.package.index')->withFlashSuccess(trans('alerts.backend.package.created'));
    }

    /**
     * Edit 
     *
     * @param HeroBanners    $package
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function edit(ManageRequest $request, Package $package)
    { 
        return view('backend.package.edit')
            ->withPackage($package);
    }

    /**
     * Update 
     *
     * @param DoubleKey    $package
     * @param UpdateRequest $request
     *
     * @return mixed
     */
    public function update(Package $package, UpdateRequest $request)
    {
        $this->validate($request, [
            'image' => 'mimes:jpg,jpeg,png|max:4096',
            ],
            $messages = [
                'mimes' => 'Only Image files are allowed (jpg, jpeg, png).'
            ]
        );

        if($request->hasFile('image')){
            $this->repository->update($package, $request->only(
                'package_id', 
                'image',
                'active'
            ));
        } else {
            $this->repository->update($package, $request->only(
                'package_id', 
                'image',
                'active'
            ));
        }
       
      
        return redirect()->route('admin.package.index')->withFlashSuccess(trans('alerts.backend.package.updated'));
    }

   
    /**
     * Get Deactivated 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageRequest $request)
    {
        return view('backend.package.deactivated');
    }

    /**
     * Get Deleted 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getDeleted(ManageRequest $request)
    {
        return view('backend.package.deleted');
    }

    /**
     * Mark
     *
     * @param $id
     * @param $status
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function mark($id, $status, ManageRequest $request)
    {
        $package = $this->repository->getHeroBannerById($id);

        $this->repository->mark($package, (int)$status);

        return redirect()->route($status == 1 ? 'admin.package.index' : 'admin.package.deactivated')->withFlashSuccess(trans('alerts.backend.package.updated'));
    }

    /**
     * Delete
     *
     * @param $id
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function delete($id, ManageRequest $request)
    {
        $package = $this->repository->findOrThrowException($id);

        $this->repository->forceDelete($package);

        return redirect()->route('admin.package.deleted')->withFlashSuccess(trans('alerts.backend.package.deleted_permanently'));
    }

    /**
     * Resote
     *
     * @param $id
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function restore($id, ManageRequest $request)
    {
        $package = $this->repository->findOrThrowException($id);

        $this->repository->restore($package);

        return redirect()->route('admin.package.index')->withFlashSuccess(trans('alerts.backend.package.restored'));
    }
}
