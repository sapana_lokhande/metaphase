<?php 

namespace App\Http\Requests\Backend\Package;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 *
 * @author Sapana Lokhande
 */

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_id'  => 'required',
            'image' => 'required',
        ];
    }
}
