<?php

namespace App\Repositories\Frontend\WebQuestionnaire;


use App\Models\Master\Attributes\MasterAttributes;
use App\Models\Questionnaire\WebQuestionnaireSubmit;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class WebQuestionnaireRepository.
 */
class WebQuestionnaireRepository extends BaseRepository
{
    public function __construct(WebQuestionnaireSubmit $model)
    {
        $this->model = $model;
    }

    public function saveWebQuestionnaire($request)
    {
        $input = array_merge(...array_values($request->get('data')));
        $attribute_stub = $this->saveWebQuestionnaireStub($input);
        $form_session = uniqid();
        $response = DB::transaction(function () use ($form_session, $attribute_stub) {
            $this->model->form_session = $form_session;
            $this->model->save();
            $this->model->web_questionnaire_value()->sync($attribute_stub);
        });
        return $response;
    }

    /**
     * @param $input
     * @param string $type
     * @return array
     */
    private function saveWebQuestionnaireStub($input, $type = 'donor_web_qes')
    {
        $allattribute = MasterAttributes::where('type', $type)->active(1)->pluck('uid', 'id')->toArray();
        $attribute_stub = [];
        foreach ($allattribute as $key => $value) {
            if (!empty($input[$value])) {
                $attribute_stub[] = ['value' => is_array($input[$value])? json_encode($input[$value]):$input[$value] , 'master_attribute_id' => $key];
            }
        }
        return $attribute_stub;
    }
}
