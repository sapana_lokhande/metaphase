<?php

namespace App\Repositories\Frontend\Attributes;


use App\Models\Master\Attributes\MasterAttributes;
use App\Repositories\BaseRepository;

/**
 * Class MasterAttributesRepository.
 */
class MasterAttributesRepository extends BaseRepository
{

    public function __construct(MasterAttributes $model)
    {
        $this->model = $model;
    }

    public function getAttributeWithOptions($input)
    {
        try {
            $attribute = $this->model->select(['id', 'name', 'title','display_title', 'uid', 'type', 'params', 'has_options', 'active'])->active()->where($input)->first();
            if (empty($attribute)) return [];
            $options = [];
            if (!empty($attribute) && $attribute->has_options == 1 && $attribute->master_attribute_option->isNotEmpty()) {
                $options = $attribute->master_attribute_option->pluck('value', 'id')->toArray();
            }
            $data = $attribute->toArray();
            unset($data['master_attribute_option']);
            $data['options'] = $options;
            return $data;
        } catch (\Exception $e) {
            return [];
        }
    }


    public function getAllAttributeWithOptions($input)
    {
        try {
            $attributes = $this->model->select(['id', 'name', 'title','display_title', 'uid', 'type', 'params', 'has_options', 'active'])->active()->where($input)->get();
            if ($attributes->isEmpty()) return [];
            $final_attributes = [];
            foreach ($attributes as $attribute) {
                $data = $options = [];
                if (!empty($attribute) && $attribute->has_options == 1 && $attribute->master_attribute_option->isNotEmpty()) {
                    $options = $attribute->master_attribute_option->pluck('value', 'id')->toArray();
                }
                $data = $attribute->toArray();
                unset($data['master_attribute_option']);
                $data['options'] = $options;
                $final_attributes[][$data['uid']] = $data;
            }
            return $final_attributes;
        } catch (\Exception $e) {
            return [];
        }
    }
}
