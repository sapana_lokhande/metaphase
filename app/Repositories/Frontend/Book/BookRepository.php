<?php

namespace App\Repositories\Frontend\Book;


use DB;
use App\Models\Book\BookTests;
use App\Models\Packages\Packages;
use App\Repositories\BaseRepository;

/**
 * Class BookRepository.
 */
class BookRepository extends BaseRepository
{
    public $model;

    public function __construct(BookTests $model)
    {
        $this->model = $model;
    }

    public function bookTest($input)
    {
        try {
            $stub = [
                'package_id' => $input['check'],
                'center_id' => $input['center'],
                'type' => $input['type'],
                'customer_name' => $input['customerName'],
                'booking_date' => $input['customerBookDT'],
                'customer_email' => $input['customerEmail'],
                'customer_phone' => $input['customerPhone'],
                'customer_note' => $input['customerNote']
            ];

            $model = $this->model->create($stub);
            return $model->id;
        } catch (\Exception $e) {
            return false;
        }
    }
}
