<?php

namespace App\Repositories\Frontend\Search;


use DB;
use App\Models\Packages\Packages;
use App\Repositories\BaseRepository;
use Illuminate\Support\Arr;

/**
 * Class SearchRepository.
 */
class SearchRepository extends BaseRepository
{
    public $packgestestlist;

    public function __construct(Packages $model)
    {
        $this->model = $model;
    }

    public function searchPackagesTests($input)
    {
        $this->getJsonByCenter($input['location']);
        $packageResult = $this->searchPackages($input['packagetest']);
        $testResult = $this->searchTests($input['packagetest']);

        $result = [
            'packageResult' => $packageResult,
            'testResult' => $testResult
        ];

        return $result;
    }

    public function getJsonByCenter($centerId)
    {
        try {
            $result = $this->model->center($centerId)->recent()->first();
            $this->packgestestlist = json_decode($result['data'], true);
        } catch (\Exception $e) {
        }
    }

    public function searchPackages($keyword)
    {
        $result = [];
		if(empty($this->packgestestlist['profileTestList']))
		{
			return $result;
        }
        
        if(empty($keyword))
        {
            return $this->packgestestlist['profileTestList'];
        }
		foreach ($this->packgestestlist['profileTestList'] as $value) {
            if (stripos($value['testName'], $keyword) !== false) {
                $result[] = $value;
            } elseif ($this->isTestAvilableInpackage($value['testList'], $keyword)) {
                $result[] = $value;
            }
        }
        return $result;
    }

    public function isTestAvilableInpackage($packageTests, $keyword)
    {

        foreach ($packageTests as $test) {
            if (stripos($test['testName'], $keyword) !== false) {
                return true;
            }
        }

        return false;
    }

    public function searchTests($keyword)
    {
        $result = [];
		if(empty($this->packgestestlist['testList']))
		{
			return $result;
        }
        
        if(empty($keyword))
        {
            return [];
            // return $this->packgestestlist['testList'];
        }

        foreach ($this->packgestestlist['testList'] as $value) {
            if (stripos($value['testName'], $keyword) !== false) {
                $result[] = $value;
            }
        }
        return $result;
    }

    public function getPackagesTests($type, $center, $check)
    {
        $this->getJsonByCenter($center);
        if ($type == 'package') {
            $result = null;
            foreach ($this->packgestestlist['profileTestList'] as $value) {
                if ($value['testID'] == $check) {
                    $result = $value;
                }
            }
            return $result;
        }
        if ($type == 'test') {
            $result = null;
            foreach ($this->packgestestlist['testList'] as $value) {
                if ($value['testID'] == $check) {
                    $result = $value;
                }
            }
            return $result;
        }
    }
    
    /**
     * Home page silder packages
     */

    public function getPackageSlider($center){
        $rand_packages = [];
        foreach ($center as $centerId => $location) {
            $this->getJsonByCenter($centerId);
            $all_packages = $this->packgestestlist['profileTestList'];
            $count = count($all_packages)>=10 ? 10:count($all_packages);
            // Use array random here and return array
            $rand_packages = Arr::random($all_packages, $count);
        }
       return $rand_packages;
        
    }
}
