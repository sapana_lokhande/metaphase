<?php

namespace App\Repositories\Frontend\Contact;


use DB;
use App\Models\Contact\Contact;
use App\Repositories\BaseRepository;

/**
 * Class ContactRepository.
 */
class ContactRepository extends BaseRepository
{
   
    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    // Create Cms Stub

    public function create(array $input): Contact
    {
     
        $data = $input;
        
        $contact = $this->createContactStub($data);
       
        return DB::transaction(function () use ($contact, $data)
            {
                if ($contact->save())
                {                    
                    return $contact;
                }
                throw new GeneralException(trans('exceptions.backend.cms.create_error'));
            });
    }

      /**
     * Create Contact Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function createContactStub($input)
    {
        $this->model->name   = $input['name'];
        $this->model->email  = $input['email'];
        $this->model->phone  = $input['phone'];
        $this->model->message = $input['message'];
      
        return $this->model;
    }

     /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->model
            ->select([
            config('tables.contact_us') . '.id',           
            config('tables.contact_us') . '.name',
            config('tables.contact_us') . '.email',
            config('tables.contact_us') . '.phone',
            config('tables.contact_us') . '.message',
            config('tables.contact_us') . '.created_at',
            config('tables.contact_us') . '.updated_at',
            config('tables.contact_us') . '.deleted_at',
        ]);

        if ($trashed == 'true')
        {
            return $dataTableQuery->onlyTrashed();
        }
        
        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }



}
