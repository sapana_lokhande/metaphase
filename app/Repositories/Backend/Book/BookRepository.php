<?php

namespace App\Repositories\Backend\Book;


use DB;
use App\Models\Book\BookTests;
use App\Models\Packages\Packages;
use App\Repositories\BaseRepository;

/**
 * Class BookRepository.
 */
class BookRepository extends BaseRepository
{
    public $model;

    public function __construct(BookTests $model)
    {
        $this->model = $model;
    }

    /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {

        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->model
            ->select([
                config('tables.bookedpt') . '.id',
                config('tables.bookedpt') . '.package_id',
                config('tables.bookedpt') . '.type',
                config('tables.bookedpt') . '.center_id',
                config('tables.bookedpt') . '.customer_name',
                config('tables.bookedpt') . '.customer_email',
                config('tables.bookedpt') . '.customer_phone',
                config('tables.bookedpt') . '.created_at',
                config('tables.bookedpt') . '.updated_at',

            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    public function bookTest($input)
    {
        try {
            $stub = [
                'package_id' => $input['check'],
                'center_id' => $input['center'],
                'type' => $input['type'],
                'customer_name' => $input['customerName'],
                'customer_email' => $input['customerEmail'],
                'customer_phone' => $input['customerPhone']
            ];

            $model = $this->model->create($stub);
            return $model->id;
        } catch (\Exception $e) {
            return false;
        }
    }
}
