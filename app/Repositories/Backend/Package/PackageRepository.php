<?php
namespace App\Repositories\Backend\Package;

/**
 * Class EloquentCmsRepository
 *
 * @author Ritesh Rathi 
 * @package App\Repositories\Backend\Package
 */
use App\Models\Package\Package;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class EloquentCmsRepository.
 */
class PackageRepository extends BaseRepository
{

     /**
     * EloquentCmsRepository constructor.
     *
     * @param  Banner  $model
     */
    public function __construct(Package $model)
    {
        $this->model = $model;
    }

    /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->model
            ->select([
            config('tables.package_images') . '.id',           
            config('tables.package_images') . '.package_id',
            config('tables.package_images') . '.image',
            config('tables.package_images') . '.active',
            config('tables.package_images') . '.created_at',
            config('tables.package_images') . '.updated_at',
            config('tables.package_images') . '.deleted_at',
        ]);

        if ($trashed == 'true')
        {
            return $dataTableQuery->onlyTrashed();
        }
        
        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

     /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * Create
     *
     * @param array $input
     */
    public function create(array $input): Package
    {
     
        $data = $input;
        
        $package = $this->createPackageStub($data);
        
        return DB::transaction(function () use ($package, $data)
            {
                if ($package->save())
                {                    
                    return $package;
                }
                throw new GeneralException(trans('exceptions.backend.package.create_error'));
            });
    }

    /**
     * Update
     *
     * @param Model $cms
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Package $package, array $input): Package
    {
        $data                   = $input;

        $package->package_id = strtolower($data['package_id']);
        $package->active            = $data['active'];
        
        if(isset($input['image']))
        {       
            $file = $input['image'];

            $package_id = rand(11111, 99999) . 'package_' . $input['package_id'] . '.' . $file->getClientOriginalExtension();
           
            $source_image_path=public_path('storage/package');
            $thumb_image_path1=public_path('storage/package/thumbs');
            $input['image']->move($source_image_path, $package_id);
            $sitehelper = new \App\Helpers\SiteHelper();
            $sitehelper->generate_image_thumbnail($source_image_path.'/'.$package_id,$thumb_image_path1.'/'.$package_id,150,150);         
            $data['image'] = $package_id; 
                     
        } else {
            if(isset($data['old_img']) && !empty($data['old_img']))
            $data['image'] = $data['old_img'];
        }
        $package->image = $data['image'];
        
        return DB::transaction(function () use ($package, $data) {
            if ($package->update([
                'package_id' => $data['package_id'],
                'image' => $data['image'],
                'active' => $data['active'],
            ])) {
             
                
                return $package;
            }

            throw new GeneralException(__('exceptions.backend.package.update_error'));

        });
    }
   

    /**
     * Mark
     *
     * @param Model $cms
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(Package $banner, $status): Package
    {
        $banner->active = $status;

        if ($banner->save())
        {
            return $banner;
        }

        throw new GeneralException(__('exceptions.backend.herobanners.mark_error'));
    }

    /**
     * Check Cms By Name 
     *
     * @param  $input
     * @param  $cms
     *
     * @throws GeneralException
     */
    protected function checkBannerByName($input, $banner)
    {

        //Figure out if name is not the same
        if ($banner->title != $input['title'])
        {
            //Check to see if name exists
            if ($this->query()->where('title', '=', $input['title'])->first())
            {
                throw new GeneralException(trans('exceptions.backend.cms.name'));
            }
        }
    }

    /**
     * Create Cms Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function createPackageStub($input)
    {
        $this->model->package_id             = $input['package_id'];
        $this->model->image              = $input['image'];
        $this->model->active            = isset($input['active']) ? 1 : 0;
        
        $file = $input['image'];

        if(isset($file))
        {            
            $package_image = rand(11111, 99999) . 'package_' . $input['package_id'] . '.' . $file->getClientOriginalExtension();
           
            $source_image_path=public_path('storage/package');
            $thumb_image_path1=public_path('storage/package/thumbs');
            $input['image']->move($source_image_path, $package_image);
            $sitehelper = new \App\Helpers\SiteHelper();
            $sitehelper->generate_image_thumbnail($source_image_path.'/'.$package_image,$thumb_image_path1.'/'.$package_image,150,150);         
            $this->model->image = $package_image;            
        }
        return $this->model;
    }

    /**
     * CmsList
     * */
    // public function cmsList()
    // {
    //     $cms = DB::table('cms')->where('active', 1)->pluck('title', 'id');
    //     return $cms;
    // }

    /**
     * getCmsByTitle
     *
     * @param $title
     */
    // public function getCmsByTitle($request)
    // {
    //     return $this->model->where('active', 1)->pluck('title');
    // }

    /**
     * getCmsById
     *
     * @param $id
     */
    public function getHeroBannerById($id)
    {
        $cms = $this->model->find($id);
        return $cms;
    }
}
