<?php
namespace App\Repositories\Backend\CmsPage;

/**
 * Class EloquentCmsRepository
 *
 * @author Ritesh Rathi 
 * @package App\Repositories\Backend\CmsPage
 */
use App\Models\CmsPage\CmsPage;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class EloquentCmsRepository.
 */
class EloquentCmsRepository extends BaseRepository
{

     /**
     * EloquentCmsRepository constructor.
     *
     * @param  CmsPage  $model
     */
    public function __construct(CmsPage $model)
    {
        $this->model = $model;
    }

    /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->model
            ->select([
            config('tables.cms_blocks') . '.id',           
            config('tables.cms_blocks') . '.title',
            config('tables.cms_blocks') . '.slug',
            config('tables.cms_blocks') . '.active',
            config('tables.cms_blocks') . '.created_at',
            config('tables.cms_blocks') . '.updated_at',
            config('tables.cms_blocks') . '.deleted_at',
        ]);

        if ($trashed == 'true')
        {
            return $dataTableQuery->onlyTrashed();
        }
        
        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

     /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * Create
     *
     * @param array $input
     */
    public function create(array $input): CmsPage
    {
     
        $data = $input;
        
        $cmspage = $this->createCmsStub($data);
       
        $this->checkCmsByName($data, $cmspage);
        
        return DB::transaction(function () use ($cmspage, $data)
            {
                if ($cmspage->save())
                {                    
                    return $cmspage;
                }
                throw new GeneralException(trans('exceptions.backend.cms.create_error'));
            });
    }

    /**
     * Update
     *
     * @param Model $cms
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(CmsPage $cms, array $input): CmsPage
    {
        $data                   = $input;        
        $cms->title             = $data['title'];

        $title = strtolower($data['title']);

        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $title);

        $cms->slug              = $slug;
        
        $cms->body              = $data['body'];

        $cms->active            = $data['active'];

        $this->checkCmsByName($data, $cms);

        return DB::transaction(function () use ($cms, $data) {
            if ($cms->update([
                'title' => $data['title'],
                'body' => $data['body'],
            ])) {
             
                return $cms;
            }

            throw new GeneralException(__('exceptions.backend.cms.update_error'));

        });
    }
    /**
     * @param Cms $cms
     *
     * @return Cms
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Cms $cms): Cms
    {
        if (is_null($cms->deleted_at))
        {
            throw new GeneralException(__('exceptions.backend.cms.delete_first'));
        }

        return DB::transaction(function () use ($cms)
            {

                if ($cms->forceDelete())
                {
                    return $cms;
                }

                throw new GeneralException(__('exceptions.backend.cms.delete_error'));
            });
    }

    /**
     * @param Cms $cms
     *
     * @return Cms
     * @throws GeneralException
     */
    public function restore(Cms $cms): Cms
    {
        if (is_null($cms->deleted_at))
        {
            throw new GeneralException(__('exceptions.backend.cms.cant_restore'));
        }

        if ($cms->restore())
        {
           // event(new CmsRestored($cms));

            return $cms;
        }

        throw new GeneralException(__('exceptions.backend.cms.restore_error'));
    }

    /**
     * Mark
     *
     * @param Model $cms
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(CmsPage $cms, $status): CmsPage
    {
        $cms->active = $status;

        // switch ($status)
        // {
        //     case 0:
        //         //event(new CmsDeactivated($cms));
        //         break;

        //     case 1:
        //         //event(new CmsReactivated($cms));
        //         break;
        // }

        if ($cms->save())
        {
            return $cms;
        }

        throw new GeneralException(__('exceptions.backend.cms.mark_error'));
    }

    /**
     * Check Cms By Name 
     *
     * @param  $input
     * @param  $cms
     *
     * @throws GeneralException
     */
    protected function checkCmsByName($input, $cms)
    {
        //Figure out if name is not the same
        // if ($cms->title != $input['title'])
        // {
            //Check to see if name exists
            if(isset($cms->id)) {
                if ($this->model->where('title', '=', $input['title'])->where('id', '!=',$cms->id)->first())
                {
                    throw new GeneralException(__('exceptions.backend.cms.name'));
                }
            } else {
                if ($this->model->where('title', '=', $input['title'])->first())
                {
                    throw new GeneralException(__('exceptions.backend.cms.name'));
                }
            }
            
        // }
    }

    /**
     * Create Cms Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function createCmsStub($input)
    {
        $this->model->title             = $input['title'];

        $title = strtolower($input['title']);
       
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $title);

        $this->model->slug              = $slug;
        $this->model->body              = $input['body'];
        $this->model->active            = isset($input['active']) ? 1 : 0;
      
        return $this->model;
    }

    /**
     * CmsList
     * */
    public function cmsList()
    {
        $cms = DB::table('cms')->where('active', 1)->pluck('title', 'id');
        return $cms;
    }

    /**
     * getCmsByTitle
     *
     * @param $title
     */
    public function getCmsByTitle($request)
    {
        return $this->model->where('active', 1)->pluck('title');
    }

    /**
     * getCmsById
     *
     * @param $id
     */
    public function getCmsById($id)
    {
        $cms = $this->model->find($id);
        return $cms;
    }
}
