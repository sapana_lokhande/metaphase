<?php

namespace App\Repositories\Backend\WebQuestionnaire;


use App\Models\Master\Attributes\MasterAttributes;
use App\Models\Questionnaire\WebQuestionnaireSubmit;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class WebQuestionnaireRepository.
 */
class WebQuestionnaireRepository extends BaseRepository
{
    public function __construct(WebQuestionnaireSubmit $model)
    {
        $this->model = $model;
    }


     /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = true, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->model
            ->select([
            config('tables.web_questionnaire_submit') . '.id',           
            config('tables.web_questionnaire_submit') . '.created_at',
            config('tables.web_questionnaire_submit') . '.updated_at',
            config('tables.web_questionnaire_submit') . '.deleted_at',
        ]);

        if ($trashed == 'true')
        {
            return $dataTableQuery->onlyTrashed();
        }
        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

     /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function saveWebQuestionnaire($request)
    {
        $input = array_merge(...array_values($request->get('data')));
        $attribute_stub = $this->saveWebQuestionnaireStub($input);
        $form_session = uniqid();
        $response = DB::transaction(function () use ($form_session, $attribute_stub) {
            $this->model->form_session = $form_session;
            $this->model->save();
            $this->model->web_questionnaire_value()->sync($attribute_stub);
        });
        return $response;
    }

    /**
     * @param $input
     * @param string $type
     * @return array
     */
    private function saveWebQuestionnaireStub($input, $type = 'donor_web_qes')
    {
        $allattribute = MasterAttributes::where('type', $type)->active(1)->pluck('uid', 'id')->toArray();
        $attribute_stub = [];
        foreach ($allattribute as $key => $value) {
            if (!empty($input[$value])) {
                $attribute_stub[] = ['value' => $input[$value], 'master_attribute_id' => $key];
            }
        }
        return $attribute_stub;
    }
}
