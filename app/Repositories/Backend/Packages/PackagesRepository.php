<?php

namespace App\Repositories\Backend\Packages;

/**
 
 * @package App\Repositories\Backend\PackagesRepository
 */

use App\Models\Packages\Packages;
use App\Repositories\BaseRepository;
use App\Models\LabCenters\LabCenters;

/**
 * Class PackagesRepository.
 */
class PackagesRepository extends BaseRepository
{

    public function __construct(
        Packages $model,
        LabCenters $labCenters
    ) {
        $this->model = $model;
        $this->labCenters = $labCenters;
    }

    public function refreshPackages($centerId)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('LIVEHEALTH_URL') . "/getAllTestsAndProfiles/?token=" . env('LIVEHEALTH_TOKEN'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $this->savePackageData($response, $centerId);
    }

    public function savePackageData($data, $centerId)
    {
        $this->model->updateOrCreate(
            ['center_id' => $centerId],
            ['data' => $data]
        );
    }

    public function getPackages()
    {
        $centerid = 1;
        $packagesTests = $this->model::Where(['active' => 1, 'center_id' => $centerid])->get();
        if ($packagesTests->isEmpty()) {
            $this->refreshPackages($centerid);
        }
        return $packagesTests;
    }

    public function fetchPackages($centerId)
    {
    }
}
