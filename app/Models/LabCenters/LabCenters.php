<?php

namespace App\Models\LabCenters;

/**
 * Class LabCenters
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LabCenters.
 */
class LabCenters extends Model
{
    use SoftDeletes;



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'token',
        'org_id',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct($args=[])
    {
        parent::__construct($args);

        $this->table = "lab_centers";
    }

    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = 1)
    {
        return $query->where('active', $status);
    }
}
