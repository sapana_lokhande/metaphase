<?php namespace App\Models\Contact;

/**
 * Class Contact
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Contact\Traits\Scope\ContactScope;
use App\Models\Contact\Traits\Relationship\ContactRelationship;

/**
 * Class Contact.
 */
class Contact extends Model
{
    use ContactScope,
        SoftDeletes,
        ContactRelationship;

        

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name', 
        'email',
        'phone',
        'message',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->table = "contact_us";
    }
}
