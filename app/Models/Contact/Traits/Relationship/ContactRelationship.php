<?php namespace App\Models\Contact\Traits\Relationship;

/**
 * Class Package
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

trait ContactRelationship
{
	  public function ContactDetails()
    {
        return $this->belongsTo('App\Models\Contact\Contact', 'id');
    }
}
