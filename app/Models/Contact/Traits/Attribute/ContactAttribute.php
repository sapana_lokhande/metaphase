<?php namespace App\Models\Contact\Traits\Attribute;
    
/**
 * Class ContactAttribute
 *
* @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

trait ContactAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) 
        {
            return "<label class='label label-success'>" . trans('labels.general.active') . '</label>';
        }

        return "<label class='label label-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active == 1;
    }

    

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->active) {
            case 0:
                return '<a href="'.route('admin.herobanners.mark', [
                    $this,
                    1,
                ]).'" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.action.activate').'"></i></a> ';
            // No break

            case 1:
                return '<a href="'.route('admin.herobanners.mark', [
                    $this,
                    0,
                ]).'" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.action.deactivate').'"></i></a> ';
            // No break

            default:
                return '';
            // No break

        }

        return '';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->restore_button.$this->delete_permanently_button;
        }

        return
            // $this->status_button.
            $this->edit_button;
            // $this->delete_button;
    }
}
