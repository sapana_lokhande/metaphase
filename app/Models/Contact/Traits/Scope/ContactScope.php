<?php namespace App\Models\Contact\Traits\Scope;

/**
 * Class Contact
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

trait ContactScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
