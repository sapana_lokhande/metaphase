<?php

namespace App\Models\Packages;

/**
 * Class Packages
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Packages.
 */
class Packages extends Model
{
    use SoftDeletes;



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'data',
        'center_id',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct($args = [])
    {
        parent::__construct($args);

        $this->table = "packages";
    }

    public function scopeActive($query, $status = 1)
    {
        return $query->where('active', $status);
    }

    public function scopeCenter($query, $centerId)
    {
        return $query->where('center_id', $centerId);
    }

    public function scopeRecent($query)
    {
        return $query->orderBy('updated_at','DESC');
    }
}
