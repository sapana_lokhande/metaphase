<?php

namespace App\Models\Book;

/**
 * Class BookTests
 *
 *
 */

use App\Models\LabCenters\LabCenters;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BookTests.
 */
class BookTests extends Model
{
    use SoftDeletes;



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'package_id',
        'center_id',
        'type',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_note',
        'booking_date',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct($args = [])
    {
        parent::__construct($args);

        $this->table = "booked_tests";
    }

    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = 1)
    {
        return $query->where('active', $status);
    }

    public function center()
    {
        return $this->belongsTo(LabCenters::class, 'center_id');
    }

}
