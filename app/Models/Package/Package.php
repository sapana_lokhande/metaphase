<?php namespace App\Models\Package;

/**
 * Class Package
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Package\Traits\Scope\PackageScope;
use App\Models\Package\Traits\Relationship\PackageRelationship;

/**
 * Class Package.
 */
class Package extends Model
{
    use PackageScope,
        SoftDeletes,
        PackageRelationship;

        

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'package_id', 
        'image',
        'active',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->table = "package_images";
    }
}
