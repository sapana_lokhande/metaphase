<?php namespace App\Models\Package\Traits\Relationship;

/**
 * Class Package
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

trait PackageRelationship
{
	  public function PackageDetails()
    {
        return $this->belongsTo('App\Models\Package\Package', 'id');
    }
}
