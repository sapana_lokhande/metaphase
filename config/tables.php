<?php

return [
    'users' => 'users',
    "cache" => "cache",
    "failed_jobs" => "failed_jobs",
    "jobs" => "jobs",
    "ledgers" => "ledgers",
    "migrations" => "migrations",
    "model_has_permissions" => "model_has_permissions",
    "model_has_roles" => "model_has_roles",
    "password_histories" => "password_histories",
    "password_resets" => "password_resets",
    "permissions" => "permissions",
    "roles" => "roles",
    "sessions" => "sessions",
    "shortlisted_profile" => "shortlisted_profile",
    "social_accounts" => "social_accounts",
    "users" => "users",
    "package_images"=>"package_images",
    "bookedpt"=>"booked_tests",
    "contact_us" => "contact_us",
];
