// @copyright_tarique

// ---------------------------slider-nav-color-change-------------------
$(document).ready(function() {
    //    var test = $('.logo-color').html();
    //    alert(test);
    $('#demo').on('slid.bs.carousel', function() {
        currentIndexPop = $('#demo div.active').index() + 1;
        if (currentIndexPop == 1) {
            $('.logo-color').addClass('take3-slider1');
            $('.btn-color').removeClass('button-bg');
            $('.logo-color').removeClass('take3-slider2');
            $('.logo-color').removeClass('take3-slider3');
            $('.toggle-icon-slide').removeClass('navbar-toggler-icon-white');
            $('.toggle-icon-slide').addClass('navbar-toggler-icon');
        }
        if (currentIndexPop == 2) {
            $('.logo-color').addClass('take3-slider2');
            $('.btn-color').addClass('button-bg');
            $('.logo-color').removeClass('take3-slider1');
            $('.logo-color').removeClass('take3-slider3');
            $('.toggle-icon-slide').addClass('navbar-toggler-icon-white');
            $('.toggle-icon-slide').removeClass('navbar-toggler-icon');
        }
        if (currentIndexPop == 3) {
            $('.logo-color').addClass('take3-slider3');
            $('.btn-color').addClass('button-bg');
            $('.logo-color').removeClass('take3-slider2');
            $('.logo-color').removeClass('take3-slider1');
            $('.toggle-icon-slide').addClass('navbar-toggler-icon-white');
            $('.toggle-icon-slide').removeClass('navbar-toggler-icon');
        }
        // alert(currentIndexPop);

    });
});
// ----------------slider-nav-color-change-closed-----------------------------

// -----------------animation------------------------------------
AOS.init();
// -----------------animation-closed--------------------------

// ---------------scroll-navigation-open------------
$(".bottomMenu").hide();

$(window).scroll(function() {
    if ($(window).scrollTop() > 50) {
        $(".bottomMenu").slideDown("show");
    } else {
        $(".bottomMenu").slideUp("hide");
    }

    if ($(window).scrollTop() > 50) {
        $(".main-nav").slideUp("hide");
    } else {
        $(".main-nav").slideDown("show");
    }

});
// ---------------scroll-navigation-closed------------
//==========upload-img======// 
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#blah')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
//======upload-img==========//

// =================got to next step=================================//
function goToNextStep(currentStep, nextStep) {
    $('.' + currentStep).hide();
    $('.' + nextStep).show();
    $('#' + currentStep).removeClass('active');
    $('#' + currentStep).addClass('done');
    $('#' + nextStep).addClass('active');

    $('html, body').animate({
        scrollTop: $(".main-form-sec").offset().top
    }, 200);
}

//==================go to previous step============================//
function goToPreviousStep(currentStep, previousStep) {
    $('.' + currentStep).hide();
    $('.' + previousStep).show();


    $('#' + currentStep).removeClass('done');
    $('#' + currentStep).removeClass('active');
    $('#' + previousStep).removeClass('done');
    $('#' + previousStep).addClass('active');

    $('html, body').animate({
        scrollTop: $(".main-form-sec").offset().top
    }, 200);
}
//==================go to ============================//
// pop-up-slider-up-down

$(document).ready(function() {
    $("#final_draft").click(function() {
        $("#mobile_toggle_nav").show();
    });

    $("#close_toggle_nav").click(function() {
        $("#mobile_toggle_nav").hide();
    });

    $("#scoll_toggle_nav").click(function() {
        // alert("dhgfhd");
        $("#scroll_mobile_toggle_nav").show();
    });

    $("#close_toggle_nav_scroll").click(function() {
        $("#scroll_mobile_toggle_nav").hide();
    });
});