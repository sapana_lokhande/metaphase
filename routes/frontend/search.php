<?php

/*
 * Frontend Access Controllers
 * All route names are prefixed with 'frontend.search'.
 */
Route::group(['namespace' => 'Search', 'as' => 'search.'], function () {


    // These routes require no user to be logged in
    Route::group(['middleware' => 'guest'], function () {
        Route::get('search', [\App\Http\Controllers\Frontend\Search\SearchController::class, 'index'])->name('package');
    });
});
