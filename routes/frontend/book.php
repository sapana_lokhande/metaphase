<?php

use App\Http\Controllers\Frontend\Book\BookController;

/*
 * Frontend Book Controllers
 * All route names are prefixed with 'frontend.book'.
 */

Route::group(['namespace' => 'Book', 'as' => 'book.'], function () {


    // These routes require no user to be logged in
    Route::group(['middleware' => 'guest'], function () {
        Route::get('book/type/{type}/center/{center}/check/{check}', [BookController::class, 'index'])->name('index');
        Route::get('details/type/{type}/center/{center}/check/{check}', [BookController::class, 'details'])->name('details');

        Route::post('book/pt', [BookController::class, 'bookPT'])->name('bookpt');
    });
});
