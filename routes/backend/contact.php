<?php

use App\Http\Controllers\Backend\Contact\ContactController;
use App\Http\Controllers\Backend\Contact\ContactTableController;

/**
* Contact
*/

Route::group(['namespace' => 'Contact'], function () {
    /*
     * For DataTables
     */
    Route::any('contact/get', 'ContactTableController')->name('contact.get');
    
    /*
     * package CRUD
     */
    Route::resource('contact', 'ContactController');

});

