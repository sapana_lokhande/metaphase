<?php

use App\Http\Controllers\Backend\Book\BookController;
use App\Http\Controllers\Backend\Book\BookTableController;

/**
 * book
 */

Route::group([
    'namespace' => 'Book',
    'middleware' => 'role:' . config('access.users.admin_role'),
], function () {
    /*
     * For DataTables
     */
    Route::any('book/get', 'BookTableController')->name('book.get');
    /*
     * book CRUD
     */
    Route::resource('book', 'BookController');

    /*
     * Deleted Book
     */
    Route::get('book/delete/{id}', 'BookController@delete')->name('book.delete-permanently');
});
