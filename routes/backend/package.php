<?php

use App\Http\Controllers\Backend\Package\PackageController;
use App\Http\Controllers\Backend\Package\PackageTableController;

/**
* package
*/

Route::group(['namespace' => 'Package'], function () {
    /*
     * For DataTables
     */
    Route::any('package/get', 'PackageTableController')->name('package.get');
    /*
     * package Status'
     */
    Route::get('package/deactivated', [PackageController::class, 'getDeactivated'])->name('package.deactivated');
    Route::get('package/deleted', [PackageController::class, 'getDeleted'])->name('package.deleted');

    /*
     * package CRUD
     */
    Route::resource('package', 'PackageController');

    // Active
    Route::get('package/mark/{id}/{status}', 'PackageController@mark')->name('package.mark')->where(['active' => '[0,1]']); 

    /*
     * Deleted Package
     */
    Route::get('package/delete/{id}', 'PackageController@delete')->name('package.delete-permanently');
    Route::get('package/restore/{id}', 'PackageController@restore')->name('package.restore');

});

