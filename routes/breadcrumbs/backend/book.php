<?php

Breadcrumbs::for('admin.book.index', function ($trail) {
	$trail->push(__('menus.backend.book.management'), route('admin.book.index'));
});

Breadcrumbs::for('admin.book.show', function ($trail, $book) {
	$trail->push(__('menus.backend.book.details'), route('admin.book.show', $book));
});
