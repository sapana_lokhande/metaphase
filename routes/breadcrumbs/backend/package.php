<?php

Breadcrumbs::for('admin.package.index', function ($trail) {
	$trail->push(__('menus.backend.package.management'), route('admin.package.index'));
});

Breadcrumbs::for('admin.package.create', function ($trail) {
    $trail->parent('admin.package.index');
    $trail->push(__('labels.backend.package.create'), route('admin.package.create'));
});

Breadcrumbs::for('admin.package.edit', function ($trail, $id) {
     $trail->parent('admin.package.index');
    $trail->push(__('menus.backend.package.edit'), route('admin.package.edit', $id));
});



